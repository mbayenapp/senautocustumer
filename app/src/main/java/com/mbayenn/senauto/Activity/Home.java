package com.mbayenn.senauto.Activity;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.config.GoogleDirectionConfiguration;
import com.akexorcist.googledirection.model.Step;
import com.arsy.maps_library.MapRipple;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mbayenn.senauto.Adapter.Locality_Adapter;
import com.mbayenn.senauto.Helpers.SessionManagerClient;
import com.mbayenn.senauto.Model.Clients;
import com.mbayenn.senauto.Model.Locality;
import com.mbayenn.senauto.Model.Points;
import com.mbayenn.senauto.Modules.DirectionFinder;
import com.mbayenn.senauto.Modules.DirectionFinderListener;
import com.mbayenn.senauto.Modules.Route;
import com.mbayenn.senauto.R;
import com.mbayenn.senauto.Utils.GPSTracker;
import com.mbayenn.senauto.Utils.GetDirectionsAsyncTask;
import com.mbayenn.senauto.provider.PlaceContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback, DirectionFinderListener, SensorEventListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    private static final String TAG = Home.class.getSimpleName();
    /*========================================================================*/
    private final String lien = "http://mapi.taxim.ma/taxi_api/";
    private final String token = "XETaxi2017";
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;
    private GoogleMap mMap;
    private GPSTracker mGPS;
    private double[] position;
    private double lattitude, longitude, lat, lng;
    private CardView card_search;
    private RelativeLayout r_section_search, r_section_adresse, r_rooll_course,
            r_search_course, r_sect_confirm_demande, r_sect_validate_demande,
            r_sect_contact_driver, r_sect_demande_accepted, r_home,
            r_sect_not_internet_connexion, r_confirm_logout, r_sect_error_home,
            r_sect_add_adress, r_sect_detail_driver, r_sect_driver_arrived, r_rate_course;
    private Animation animSideUp, animSideRoll, animSlideUp, animSlideDown, animRollDemande, animSideFadeOut, animFadeIn;
    private ImageView img_go_to_back, img_menu_not_connexion, img_back_detail,
            img_contact_driver_arrived;
    private Button btn_roll_course, btn_no_roll, btn_validate_commande,
            btn_roll_demande_driver, btn_contact_driver, btn_reload,
            btn_no_logout, btn_yes_logout, btn_not_confirm,
            btn_confirm_commande, btn_roll, btn_call_driver,
            btn_roll_course_arrived;
    private Intent intent;
    private Marker ourGlobalMarker;
    private Location location;
    private Marker marker;
    private Polyline polylinePaths = null;
    private List<Points> mayTatlngs = new ArrayList<>();
    private Handler myHandler;
    private Marker originMarkers = null;
    private Marker destinationMarkers = null;
    private boolean isMarkerRotating = false;
    private LatLng pos_custumer;
    private Points pitrajet;
    private PolylineOptions polylineOptions;
    private LatLng position_driver;
    private int zoom0 = 0, zoom1 = 0, zoom2 = 0, zoom3 = 0, zoom4 = 0, zoom5 = 0, zoom6 = 0, zoom7 = 0, zoom8 = 0, zoom9 = 0;
    private int niv_back = 0;
    private LinearLayout l_reload, l_confirm_logout;
    private Toolbar toolbar;
    private ProgressBar prog_reload;
    private SessionManagerClient sessionManagerClient;
    private View navHeader;
    private String link = "";
    private CircleImageView img_profil_client;
    /*private Clients cl;*/
    private TextView txt_map_incator, txt_my_position_arrive,
            txt_adresse_dep_demande, txt_adresse_arrv_demande,
            name_driver_detail;
    private ImageView imageMarker, img_close_r_layout,
            img_see_detail_driver;
    private LatLng mCenterLatLong;
    private List<Integer> distanceList = new ArrayList<>();
    private List<String> maneuverList = new ArrayList<>();
    private List<Integer> timeList = new ArrayList<>();
    private List<LatLng> pointList = new ArrayList<>();
    private Handler myHandlerdirection;
    private Boolean init_map = false;
    List<Step> stepList = new ArrayList<>();
    private float mDeclination;
    private float[] mRotationMatrix = new float[9];
    private float altitude;
    private TextView txt_my_position, txt_nothing_adress,
            txt_add_adr_home_user, txt_adr_home_user,
            txt_add_adr_work_user, txt_adr_work_user,
            txt_nothing_add_adress, txt_my_add_adress,
            txt_name_driver_accepted, txt_etoile_driver_accepted,
            txt_adresse_arrv_demande_arrived, xt_adresse_dep_demande_arrived,
            txt_name_driver_arrived;

    private static final int PERMISSIONS_REQUEST_FINE_LOCATION = 111;
    private static final int PLACE_PICKER_REQUEST = 1;
    private GoogleApiClient mClient;
    private PlaceListAdapter mAdapter;
    private String KEY = "AIzaSyAUkyEG9duEFiVRqtWc3U8iHqNZDopIlQI";
    private String URL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=";
    private List<Locality> localities = new ArrayList<>();
    private Locality locality;
    private ListView list_adresse, list_add_adresse;
    private Locality_Adapter adapter;
    private ProgressBar progress_wait, progress_wait_add;
    private String ville = "";
    private RelativeLayout r_my_loader;
    private int init_polyline = 0;
    private String user_id = "", GpsDepart = "", AdresseDepart = "", GpsArrivee = "", AdresseArrivee = "", Distance = "", Duree = "", Type = "", Etat = "";
    private int init_loader = 0;
    private String gpsDepart = "", getGpsArrivee = "";
    private String inc;
    private int permissionCheeck;
    private MaterialRatingBar score_chauffeur, score_chauffeur_arrived, rate_chauffeur;
    private int init_detail_chauffeur = 0;
    private LinearLayout l_sect_cancel_ride;
    private Button btn_not_confirm_rate, btn_validate_rate;
    private String Key_dem = "";
    private String etat = "";
    private DatabaseReference new_demande;
    /*========================================================================*/
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseUser user;
    private int pos_inc = 0;
    private String type_adresse = "";
    private int init_arc = 0;
    private int init_dem_encours = 0;
    private String status_dem = "";
    private Handler myHandlerCours;
    private String tel_driver = "";
    private MapRipple mapRipple = null;
    /*==================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        /*==================================================================*/
        myHandler = new Handler();
        myHandler.postDelayed(myRunPolyline, 5);
        /*==================================================================*/
        initAnimation();
        rollAnimation();
        SlideUpAnimation();
        SlideDownAnimation();
        RollDemandeAnimation();
        fadeOutAnime();
        fadeinAnime();
        sessionManagerClient = new SessionManagerClient(this);
        /*==================================================================*/
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        permissionCheeck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        /*=================================================================*/
        card_search = (CardView) findViewById(R.id.card_search);
        r_section_search = (RelativeLayout) findViewById(R.id.r_section_search);
        img_go_to_back = (ImageView) findViewById(R.id.img_go_to_back);
        r_section_adresse = (RelativeLayout) findViewById(R.id.r_section_adresse);
        r_rooll_course = (RelativeLayout) findViewById(R.id.r_rooll_course);
        btn_roll_course = (Button) findViewById(R.id.btn_roll_course);
        btn_no_roll = (Button) findViewById(R.id.btn_no_roll);
        r_search_course = (RelativeLayout) findViewById(R.id.r_search_course);
        r_sect_confirm_demande = (RelativeLayout) findViewById(R.id.r_sect_confirm_demande);
        r_sect_validate_demande = (RelativeLayout) findViewById(R.id.r_sect_validate_demande);
        btn_validate_commande = (Button) findViewById(R.id.btn_validate_commande);
        btn_roll_demande_driver = (Button) findViewById(R.id.btn_roll_demande_driver);
        btn_contact_driver = (Button) findViewById(R.id.btn_contact_driver);
        r_sect_contact_driver = (RelativeLayout) findViewById(R.id.r_sect_contact_driver);
        r_sect_demande_accepted = (RelativeLayout) findViewById(R.id.r_sect_demande_accepted);
        r_home = (RelativeLayout) findViewById(R.id.r_home);
        r_sect_not_internet_connexion = (RelativeLayout) findViewById(R.id.r_sect_not_internet_connexion);
        img_menu_not_connexion = (ImageView) findViewById(R.id.img_menu_not_connexion);
        btn_reload = (Button) findViewById(R.id.btn_reload);
        l_reload = (LinearLayout) findViewById(R.id.l_reload);
        prog_reload = (ProgressBar) findViewById(R.id.prog_reload);
        l_confirm_logout = (LinearLayout) findViewById(R.id.l_confirm_logout);
        r_confirm_logout = (RelativeLayout) findViewById(R.id.r_confirm_logout);
        btn_no_logout = (Button) findViewById(R.id.btn_no_logout);
        btn_yes_logout = (Button) findViewById(R.id.btn_yes_logout);
        r_sect_error_home = (RelativeLayout) findViewById(R.id.r_sect_error_home);
        txt_map_incator = (TextView) findViewById(R.id.locationMarkertext);
        imageMarker = (ImageView) findViewById(R.id.imagemarker);
        txt_my_position_arrive = (TextView) findViewById(R.id.txt_my_position_arrive);
        list_adresse = (ListView) findViewById(R.id.list_adresse);
        txt_nothing_adress = (TextView) findViewById(R.id.txt_nothing_adress);
        progress_wait = (ProgressBar) findViewById(R.id.progress_wait);
        r_my_loader = (RelativeLayout) findViewById(R.id.r_my_loader);
        btn_not_confirm = (Button) findViewById(R.id.btn_not_confirm);
        btn_confirm_commande = (Button) findViewById(R.id.btn_confirm_commande);
        txt_adresse_dep_demande = (TextView) findViewById(R.id.txt_adresse_dep_demande);
        txt_adresse_arrv_demande = (TextView) findViewById(R.id.txt_adresse_arrv_demande);
        btn_roll = (Button) findViewById(R.id.btn_roll);
        r_sect_add_adress = (RelativeLayout) findViewById(R.id.r_sect_add_adress);
        txt_add_adr_home_user = (TextView) findViewById(R.id.txt_add_adr_home_user);
        txt_adr_home_user = (TextView) findViewById(R.id.txt_adr_home_user);
        txt_add_adr_work_user = (TextView) findViewById(R.id.txt_add_adr_work_user);
        txt_adr_work_user = (TextView) findViewById(R.id.txt_adr_work_user);
        txt_nothing_add_adress = (TextView) findViewById(R.id.txt_nothing_add_adress);
        txt_my_add_adress = (TextView) findViewById(R.id.txt_my_add_adress);
        list_add_adresse = (ListView) findViewById(R.id.list_add_adresse);
        progress_wait_add = (ProgressBar) findViewById(R.id.progress_wait_add);
        txt_name_driver_accepted = (TextView) findViewById(R.id.txt_name_driver_accepted);
        txt_etoile_driver_accepted = (TextView) findViewById(R.id.txt_etoile_driver_accepted);
        img_close_r_layout = (ImageView) findViewById(R.id.img_close_r_layout);
        btn_call_driver = (Button) findViewById(R.id.btn_call_driver);
        r_sect_detail_driver = (RelativeLayout) findViewById(R.id.r_sect_detail_driver);
        img_see_detail_driver = (ImageView) findViewById(R.id.img_see_detail_driver);
        name_driver_detail = (TextView) findViewById(R.id.name_driver_detail);
        score_chauffeur = (MaterialRatingBar) findViewById(R.id.score_chauffeur);
        img_back_detail = (ImageView) findViewById(R.id.img_back_detail);
        r_sect_driver_arrived = (RelativeLayout) findViewById(R.id.r_sect_driver_arrived);
        txt_adresse_arrv_demande_arrived = (TextView) findViewById(R.id.txt_adresse_arrv_demande_arrived);
        xt_adresse_dep_demande_arrived = (TextView) findViewById(R.id.txt_adresse_dep_demande_arrived);
        txt_name_driver_arrived = (TextView) findViewById(R.id.txt_name_driver_arrived);
        score_chauffeur_arrived = (MaterialRatingBar) findViewById(R.id.score_chauffeur_arrived);
        img_contact_driver_arrived = (ImageView) findViewById(R.id.img_contact_driver_arrived);
        btn_roll_course_arrived = (Button) findViewById(R.id.btn_roll_course_arrived);
        l_sect_cancel_ride = (LinearLayout) findViewById(R.id.l_sect_cancel_ride);
        r_rate_course = (RelativeLayout) findViewById(R.id.r_rate_course);
        rate_chauffeur = (MaterialRatingBar) findViewById(R.id.rate_chauffeur);
        btn_not_confirm_rate = (Button) findViewById(R.id.btn_not_confirm_rate);
        btn_validate_rate = (Button) findViewById(R.id.btn_validate_rate);
        /*=================================================================================*/
        mAuth = FirebaseAuth.getInstance();
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = FirebaseAuth.getInstance().getCurrentUser();
            }
        };
        DatabaseReference d_inc = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Clients").child(mAuth.getCurrentUser().getUid());
        d_inc.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Key_dem = dataSnapshot.getKey() + "_" + dataSnapshot.child("increment").getValue().toString();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*=================================================================================*/
        d_inc.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                inc = dataSnapshot.child("increment").getValue().toString();
                final DatabaseReference new_demande = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes").child(mAuth.getCurrentUser().getUid() + "_" + inc);
                new_demande.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            String etat = dataSnapshot.child("Etat").getValue().toString();
                            Key_dem = dataSnapshot.getKey();
                            if (!etat.equals("Demande closed")) {
                                if (etat.equals("Nouvelle demande")) {
                                    status_dem = "nouvelle";
                                    init_arc = 1;
                                    init_dem_encours = 1;
                                    toolbar.setVisibility(View.GONE);
                                    card_search.setVisibility(View.GONE);
                                    r_my_loader.setVisibility(View.GONE);
                                    r_search_course.setVisibility(View.VISIBLE);
                                    mMap.getUiSettings().setAllGesturesEnabled(false);
                                    mMap.getUiSettings().setZoomGesturesEnabled(false);
                                    mMap.getUiSettings().setScrollGesturesEnabled(false);
                                    mMap.getUiSettings().setMapToolbarEnabled(false);
                                    mMap.getUiSettings().setRotateGesturesEnabled(false);
                                    //
                                    txt_adresse_dep_demande.setText(dataSnapshot.child("AdresseDepart").getValue().toString());
                                    txt_adresse_arrv_demande.setText(dataSnapshot.child("AdresseArrivee").getValue().toString());

                                    if (isNetworkAvailable(Home.this)) {
                                        if (mGPS.canGetLocation) {
                                            mGPS.getLocation();
                                            lattitude = mGPS.getLatitude();
                                            longitude = mGPS.getLongitude();

                                            HashMap<String, Object> resPosition = new HashMap<>();
                                            resPosition.put("Lattitude", lattitude);
                                            resPosition.put("Longitude", longitude);
                                            new_demande.updateChildren(resPosition);
                                        }
                                    }
                                } else if (etat.equals("Demande accepted")) {
                                    init_arc = 1;
                                    if (init_loader == 0) {
                                        myHandlerCours = new Handler();
                                        myHandlerCours.postDelayed(myRunMyPoliline, 5);
                                        init_loader = 1;
                                        init_dem_encours = 1;
                                        //r_my_loader.setVisibility(View.VISIBLE);
                                        gpsDepart = dataSnapshot.child("GpsDepart").getValue().toString();
                                        getGpsArrivee = dataSnapshot.child("GpsArrivee").getValue().toString();
                                        init_polyline = 0;
                                        r_my_loader.setVisibility(View.GONE);
                                        r_search_course.setVisibility(View.GONE);
                                        r_sect_demande_accepted.setVisibility(View.VISIBLE);
                                        r_sect_validate_demande.setVisibility(View.GONE);
                                        toolbar.setVisibility(View.GONE);
                                        card_search.setVisibility(View.GONE);
                                        mMap.getUiSettings().setMapToolbarEnabled(false);
                                        mMap.getUiSettings().setRotateGesturesEnabled(false);
                                        mMap.getUiSettings().setAllGesturesEnabled(true);
                                        mMap.setBuildingsEnabled(true);
                                        mMap.setMinZoomPreference(6.0f);
                                        mMap.setMaxZoomPreference(18.0f);
                                    }
                                } else if (etat.equals("Driver arrived")) {
                                    init_arc = 1;
                                    if (init_loader == 0) {
                                        myHandlerCours = new Handler();
                                        myHandlerCours.postDelayed(myRunMyPoliline, 5);
                                        init_loader = 1;
                                        init_dem_encours = 1;
                                        init_polyline = 0;
                                        r_my_loader.setVisibility(View.GONE);
                                        r_search_course.setVisibility(View.GONE);
                                        r_sect_demande_accepted.setVisibility(View.GONE);
                                        r_sect_validate_demande.setVisibility(View.GONE);
                                        r_sect_driver_arrived.setVisibility(View.VISIBLE);
                                        toolbar.setVisibility(View.GONE);
                                        card_search.setVisibility(View.GONE);
                                        mMap.getUiSettings().setMapToolbarEnabled(false);
                                        mMap.getUiSettings().setRotateGesturesEnabled(false);
                                        mMap.getUiSettings().setAllGesturesEnabled(true);
                                        mMap.setBuildingsEnabled(true);
                                        mMap.setMinZoomPreference(6.0f);
                                        mMap.setMaxZoomPreference(18.0f);
                                        txt_adresse_arrv_demande_arrived.setText(dataSnapshot.child("AdresseArrivee").getValue().toString());
                                        xt_adresse_dep_demande_arrived.setText(dataSnapshot.child("AdresseDepart").getValue().toString());
                                    }
                                } else if (etat.equals("Custumer at board")) {
                                    init_arc = 1;
                                    if (init_loader == 0) {
                                        myHandlerCours = new Handler();
                                        myHandlerCours.postDelayed(myRunMyPoliline, 5);
                                        init_loader = 1;
                                        init_dem_encours = 1;
                                        init_polyline = 0;
                                        r_my_loader.setVisibility(View.GONE);
                                        r_search_course.setVisibility(View.GONE);
                                        r_sect_demande_accepted.setVisibility(View.GONE);
                                        r_sect_validate_demande.setVisibility(View.GONE);
                                        r_sect_driver_arrived.setVisibility(View.VISIBLE);
                                        l_sect_cancel_ride.setVisibility(View.GONE);
                                        toolbar.setVisibility(View.GONE);
                                        card_search.setVisibility(View.GONE);
                                        mMap.getUiSettings().setMapToolbarEnabled(false);
                                        mMap.getUiSettings().setRotateGesturesEnabled(false);
                                        mMap.getUiSettings().setAllGesturesEnabled(true);
                                        mMap.setBuildingsEnabled(true);
                                        mMap.setMinZoomPreference(6.0f);
                                        mMap.setMaxZoomPreference(18.0f);
                                        txt_adresse_arrv_demande_arrived.setText(dataSnapshot.child("AdresseArrivee").getValue().toString());
                                        xt_adresse_dep_demande_arrived.setText(dataSnapshot.child("AdresseDepart").getValue().toString());
                                    }
                                } else if (etat.equals("Custumer deposed")) {
                                    init_arc = 1;
                                    if (init_loader == 0) {
                                        init_loader = 1;
                                        init_dem_encours = 1;
                                        init_polyline = 0;
                                        r_rate_course.setVisibility(View.VISIBLE);
                                        r_my_loader.setVisibility(View.GONE);
                                        r_search_course.setVisibility(View.GONE);
                                        r_sect_demande_accepted.setVisibility(View.GONE);
                                        r_sect_validate_demande.setVisibility(View.GONE);
                                        r_sect_driver_arrived.setVisibility(View.GONE);
                                        l_sect_cancel_ride.setVisibility(View.GONE);
                                        toolbar.setVisibility(View.GONE);
                                        card_search.setVisibility(View.GONE);
                                        mMap.clear();
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*=================================================================================*/

        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navHeader = navigationView.getHeaderView(0);
        img_profil_client = (CircleImageView) navHeader.findViewById(R.id.img_user_on_line);
        final TextView txt_name_client = (TextView) navHeader.findViewById(R.id.txt_name_client);
        final TextView txt_email_client = (TextView) navHeader.findViewById(R.id.txt_email_client);
        final LinearLayout l_user_on_line = (LinearLayout) navHeader.findViewById(R.id.l_user_on_line);
        final TextView txt_logout = (TextView) navHeader.findViewById(R.id.txt_logout);
        txt_my_position = (TextView) findViewById(R.id.txt_my_position);
        /*=================================================================*/
        r_home.startAnimation(animFadeIn);
        /*=================================================================*/
        if (mAuth.getCurrentUser() != null) {
            String user_id = mAuth.getCurrentUser().getUid();
            DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Clients").child(user_id);
            txt_email_client.setText(mAuth.getCurrentUser().getEmail());
            String prenom = "", nom = "";
            current_user_db.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    txt_name_client.setText(dataSnapshot.child("prenom").getValue().toString() + " " + dataSnapshot.child("nom").getValue().toString());
                    //inc = dataSnapshot.child("increment").getValue().toString();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        /*
        * Vérifier s'il y'a une nouvelle demande
        * */
        /*DatabaseReference new_demande = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes").child(mAuth.getCurrentUser().getUid());
        new_demande.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String etat = dataSnapshot.child("Etat").getValue().toString();
                    if (etat.equals("Nouvelle demande")){
                        toolbar.setVisibility(View.GONE);
                        card_search.setVisibility(View.GONE);
                        r_my_loader.setVisibility(View.GONE);
                        r_search_course.setVisibility(View.VISIBLE);
                        mMap.getUiSettings().setAllGesturesEnabled(false);
                        mMap.getUiSettings().setZoomGesturesEnabled(false);
                        mMap.getUiSettings().setScrollGesturesEnabled(false);
                        mMap.getUiSettings().setMapToolbarEnabled(false);
                        mMap.getUiSettings().setRotateGesturesEnabled(false);
                        //
                        txt_adresse_dep_demande.setText(dataSnapshot.child("AdresseDepart").getValue().toString());
                        txt_adresse_arrv_demande.setText(dataSnapshot.child("AdresseArrivee").getValue().toString());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
        /*
        * Fin vérification nouvelle demande
        * */
        /*=================================================================*/
        /*
        * Gestion des adresse de domicile et de travail
        * */
        DatabaseReference d_adresse_domicile = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Favoris").child(mAuth.getCurrentUser().getUid());
        DatabaseReference d_adresse_travail = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Favoris").child(mAuth.getCurrentUser().getUid());
        d_adresse_domicile.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("domicile").exists()) {
                    txt_add_adr_home_user.setVisibility(View.GONE);
                    txt_adr_home_user.setVisibility(View.VISIBLE);
                    txt_adr_home_user.setText(dataSnapshot.child("domicile").child("Adresse").getValue().toString());
                } else {
                    txt_add_adr_home_user.setVisibility(View.VISIBLE);
                    txt_adr_home_user.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Aucune adresse n'a été trouvée", Toast.LENGTH_LONG).show();
            }
        });
        d_adresse_travail.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("travail").exists()) {
                    txt_add_adr_work_user.setVisibility(View.GONE);
                    txt_adr_work_user.setVisibility(View.VISIBLE);
                    txt_adr_work_user.setText(dataSnapshot.child("travail").child("Adresse").getValue().toString());
                } else {
                    txt_add_adr_work_user.setVisibility(View.VISIBLE);
                    txt_adr_work_user.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Aucune adresse n'a été trouvée", Toast.LENGTH_LONG).show();
            }
        });
        /*=================================================================*/
        /*
        * Action des ciques sur les adresse favoties
        * */
        txt_adr_home_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String adr = txt_adr_home_user.getText().toString().trim() + " " + ville;
                AdresseArrivee = txt_adr_home_user.getText().toString().trim();

                r_section_search.setVisibility(View.GONE);
                r_my_loader.setVisibility(View.VISIBLE);
                card_search.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txt_my_position_arrive, InputMethodManager.HIDE_IMPLICIT_ONLY);
                String dep = lat + "," + lng;
                sendRequest(dep, adr);
            }
        });
        txt_adr_work_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String adr = txt_adr_work_user.getText().toString().trim() + " " + ville;
                AdresseArrivee = txt_adr_work_user.getText().toString().trim();

                r_section_search.setVisibility(View.GONE);
                r_my_loader.setVisibility(View.VISIBLE);
                card_search.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txt_my_position_arrive, InputMethodManager.HIDE_IMPLICIT_ONLY);
                String dep = lat + "," + lng;
                sendRequest(dep, adr);
            }
        });
        /*=================================================================*/
        //Key_dem = "hwUNNN08rkhuyVJeBfw7IOnp3cG2_28";
        DatabaseReference d_prop = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Clients").child(mAuth.getCurrentUser().getUid());
        d_prop.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Key_dem = dataSnapshot.getKey() + "_" + dataSnapshot.child("increment").getValue().toString();
                    if (!Key_dem.equals("")) {
                        final DatabaseReference dt_dem = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes").child(Key_dem);
                        final HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("Etat", "Demande closed");
                        final DatabaseReference dt_course = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Courses").child(Key_dem);
                        final HashMap<String, Object> h_course = new HashMap<>();
                        btn_validate_rate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                float scrore = rate_chauffeur.getRating();
                                if (scrore != 0.0) {
                                    r_my_loader.setVisibility(View.VISIBLE);
                                    r_rate_course.setVisibility(View.GONE);
                                    h_course.put("Scrore", String.valueOf(scrore));
                                    dt_course.updateChildren(h_course);
                                    dt_dem.updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                startActivity(new Intent(getApplicationContext(), Home.class));
                                                finish();
                                            }
                                        }
                                    });
                                } else {
                                    Toast.makeText(getApplicationContext(), "Vous devez indiquer votre cote", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                        btn_not_confirm_rate.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                r_my_loader.setVisibility(View.VISIBLE);
                                r_rate_course.setVisibility(View.GONE);
                                h_course.put("Scrore", "1");
                                dt_course.updateChildren(h_course);
                                dt_dem.updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            startActivity(new Intent(getApplicationContext(), Home.class));
                                            finish();
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*=================================================================*/
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        if (isNetworkAvailable(Home.this)) {
            mGPS = new GPSTracker(this);
            position = new double[2];
            if (mGPS.canGetLocation()) {
                mGPS.getLocation();
                lat = mGPS.getLatitude();
                lng = mGPS.getLongitude();
            }
            /*==================================================================*/
            mClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .enableAutoManage(this, this)
                    .build();
            /*cl = get_client(sessionManagerClient.getLoggedInClient());
            txt_name_client.setText(cl.getPrenom()+" "+cl.getNom());
            txt_email_client.setText(cl.getEmail());
            if (!cl.getPhoto().equals("null")) {
                link = lien+"webroot/clients/" + cl.getPhoto();
            } else {
                link = lien+"webroot/clients/boy.jpg";
            }
            Glide.with(getApplicationContext()).load(link).fitCenter().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_profil_client);*/
            /*==================================================================*/
            //sendRequest(lat + "," + lng, "Residence Tachefine Marrakech");
            l_user_on_line.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawers();
                    intent = new Intent(getApplicationContext(), Profil.class);
                    r_home.startAnimation(animSideFadeOut);
                }
            });
        } else {
            toolbar.setVisibility(View.GONE);
            card_search.setVisibility(View.GONE);
            r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
            r_sect_not_internet_connexion.startAnimation(animFadeIn);
        }

        /*===================================================================*/
        /*SupportMapFragment mapFragment_drag = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment_drag.getMapAsync(this);*/
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_home);
        mapFragment.getMapAsync(this);
        /*==================================================================*/
        img_close_r_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_sect_contact_driver.setVisibility(View.GONE);
            }
        });
        img_contact_driver_arrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_sect_contact_driver.setVisibility(View.VISIBLE);
            }
        });
        btn_call_driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (permissionCheeck != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(Home.this, Manifest.permission.READ_PHONE_STATE)) {
                        ActivityCompat.requestPermissions(Home.this,
                                new String[]{Manifest.permission.READ_PHONE_STATE},
                                MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                    } else {
                        ActivityCompat.requestPermissions(Home.this,
                                new String[]{Manifest.permission.READ_PHONE_STATE},
                                MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                    }
                } else {
                    Intent it = new Intent(Intent.ACTION_CALL);
                    it.setData(Uri.parse("tel:" + tel_driver));
                    if (ActivityCompat.checkSelfPermission(Home.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    r_sect_contact_driver.setVisibility(View.GONE);
                    startActivity(it);
                }
            }
        });
        img_see_detail_driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                niv_back = 1;
                init_detail_chauffeur = 1;
                r_sect_detail_driver.setVisibility(View.VISIBLE);
                //r_sect_detail_driver.startAnimation(animFadeIn);
            }
        });
        img_back_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_sect_detail_driver.setVisibility(View.GONE);
            }
        });
        txt_add_adr_home_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.setVisibility(View.GONE);
                card_search.setVisibility(View.GONE);
                r_sect_add_adress.setVisibility(View.VISIBLE);
                r_sect_add_adress.startAnimation(animFadeIn);
                r_section_search.setVisibility(View.GONE);
                txt_my_add_adress.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txt_add_adr_home_user, InputMethodManager.SHOW_IMPLICIT);
                type_adresse = "domicile";
                niv_back = 1;
            }
        });
        txt_add_adr_work_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.setVisibility(View.GONE);
                card_search.setVisibility(View.GONE);
                r_sect_add_adress.setVisibility(View.VISIBLE);
                r_sect_add_adress.startAnimation(animFadeIn);
                r_section_search.setVisibility(View.GONE);
                txt_my_add_adress.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txt_my_add_adress, InputMethodManager.SHOW_IMPLICIT);
                type_adresse = "travail";
                niv_back = 1;
            }
        });
        card_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.setVisibility(View.GONE);
                r_section_search.setVisibility(View.VISIBLE);
                r_section_search.startAnimation(animSideUp);
                r_section_adresse.setVisibility(View.VISIBLE);
                r_section_adresse.startAnimation(animSlideUp);
                txt_my_position_arrive.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(txt_my_position_arrive, InputMethodManager.SHOW_IMPLICIT);
                niv_back = 1;
            }
        });
        img_go_to_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_section_search.setVisibility(View.GONE);
                //r_section_search.startAnimation(animSideRoll);
                toolbar.setVisibility(View.VISIBLE);
                toolbar.startAnimation(animSideUp);
                r_section_adresse.setVisibility(View.GONE);
                r_section_adresse.startAnimation(animSlideDown);
            }
        });
        txt_my_add_adress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String cheq = String.valueOf(s);
                if (!cheq.equals("")) {
                    progress_wait_add.setVisibility(View.VISIBLE);
                    final OkHttpClient client = new OkHttpClient();
                    RequestBody body = new FormBody.Builder()
                            .add("text", cheq)
                            .build();

                    final Request request = new Request.Builder()
                            .url(URL + cheq + "&types=geocode&location=" + lat + "," + lng + "&radius=500000&strictbounds&language=fr&key=" + KEY)
                            .addHeader("Content-Type", "text/json; Charset=UTF-8")
                            .build();
                    AsyncTask<Void, Void, JSONObject> asyncTask = new AsyncTask<Void, Void, JSONObject>() {
                        @Override
                        protected JSONObject doInBackground(Void... params) {
                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                            StrictMode.setThreadPolicy(policy);
                            Response response = null;
                            JSONObject jsonObject = null;
                            try {
                                response = client.newCall(request).execute();
                                if (!response.isSuccessful()) {
                                    return null;
                                } else {
                                    jsonObject = new JSONObject(response.body().string());
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            return jsonObject;
                        }

                        @Override
                        protected void onPostExecute(JSONObject jsonObject) {
                            super.onPostExecute(jsonObject);
                            try {
                                JSONArray j_prediction = jsonObject.getJSONArray("predictions");
                                Log.d(TAG, "Reponse: " + j_prediction);
                                Log.d(TAG, "nombre: " + j_prediction.length());
                                if (j_prediction.length() != 0) {
                                    JSONObject j_section = null;
                                    localities.clear();
                                    for (int i = 0; i < j_prediction.length(); i++) {
                                        j_section = j_prediction.getJSONObject(i);
                                        String[] p = j_section.optString("description").split(",");
                                        JSONArray j_terms = j_section.getJSONArray("terms");
                                        JSONObject j_ville = (JSONObject) j_terms.get(1);
                                        JSONObject j_pays = (JSONObject) j_terms.get(2);
                                        Log.d(TAG, "Terms: " + j_terms);
                                        Log.d(TAG, "Ville: " + j_ville);
                                        Log.d(TAG, "Pays: " + j_pays);
                                        Log.d(TAG, "Description: " + j_section.optString("description"));
                                        Log.d(TAG, "id: " + j_section.optString("id"));
                                        if (j_section != null && j_ville != null && j_pays != null) {
                                            list_add_adresse.setVisibility(View.VISIBLE);
                                            txt_nothing_adress.setVisibility(View.GONE);
                                            locality = new Locality();
                                            locality.setId(j_section.optString("id"));
                                            locality.setAdresse(p[0]);
                                            locality.setVille(j_ville.optString("value"));
                                            locality.setPays(j_pays.optString("value"));
                                            localities.add(locality);
                                            adapter = new Locality_Adapter(getApplicationContext(), localities);
                                            adapter.notifyDataSetChanged();
                                            list_add_adresse.setAdapter(adapter);
                                            progress_wait_add.setVisibility(View.GONE);
                                            setListViewHeightBasedOnChildren(list_add_adresse);
                                        }
                                    }
                                } else {
                                    progress_wait_add.setVisibility(View.GONE);
                                    txt_nothing_add_adress.setVisibility(View.VISIBLE);
                                    list_add_adresse.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    asyncTask.execute();
                } else {
                    progress_wait_add.setVisibility(View.GONE);
                    txt_nothing_add_adress.setVisibility(View.VISIBLE);
                    list_add_adresse.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        list_add_adresse.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Locality locality = localities.get(position);
                r_my_loader.setVisibility(View.VISIBLE);
                r_sect_add_adress.setVisibility(View.GONE);
                final DatabaseReference dadresse = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Favoris").child(mAuth.getCurrentUser().getUid()).child(type_adresse);
                dadresse.child("Adresse").setValue(locality.getAdresse());
                dadresse.child("Ville").setValue(locality.getVille());
                dadresse.child("Pays").setValue(locality.getPays());
                new CountDownTimer(1000, 500) {

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        r_my_loader.setVisibility(View.GONE);
                        //toolbar.setVisibility(View.VISIBLE);
                        card_search.setVisibility(View.VISIBLE);
                        r_sect_add_adress.startAnimation(animFadeIn);
                        r_section_search.setVisibility(View.VISIBLE);
                    }
                }.start();
            }
        });
        /*=================================================================*/
        //Annuler la course
        btn_roll_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_my_loader.setVisibility(View.VISIBLE);
                new CountDownTimer(200, 50) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        r_my_loader.setVisibility(View.GONE);
                        r_rooll_course.setVisibility(View.VISIBLE);
                        r_rooll_course.startAnimation(animSideUp);
                    }
                }.start();
            }
        });
        btn_roll_course_arrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_my_loader.setVisibility(View.VISIBLE);
                new CountDownTimer(200, 50) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        r_my_loader.setVisibility(View.GONE);
                        r_rooll_course.setVisibility(View.VISIBLE);
                        r_rooll_course.startAnimation(animSideUp);
                    }
                }.start();
            }
        });
        btn_roll_demande_driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_rooll_course.setVisibility(View.VISIBLE);
                r_rooll_course.startAnimation(animSideUp);
            }
        });
        btn_no_roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_rooll_course.setVisibility(View.GONE);
                r_rooll_course.startAnimation(animRollDemande);
            }
        });
        btn_validate_commande.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_sect_validate_demande.setVisibility(View.GONE);
                r_sect_confirm_demande.setVisibility(View.VISIBLE);
                r_sect_confirm_demande.startAnimation(animSideUp);
            }
        });
        btn_contact_driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_sect_contact_driver.setVisibility(View.VISIBLE);
                r_sect_contact_driver.startAnimation(animSideUp);
                //r_sect_demande_accepted.setVisibility(View.GONE);
            }
        });

        img_menu_not_connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.openDrawer(GravityCompat.START);
            }
        });
        btn_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                l_reload.setVisibility(View.GONE);
                prog_reload.setVisibility(View.VISIBLE);
                new CountDownTimer(2000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        startActivity(new Intent(getApplicationContext(), Home.class));
                        finish();
                    }
                }.start();
            }
        });
        btn_not_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_sect_validate_demande.setVisibility(View.GONE);
                mMap.clear();
                init_polyline = 0;
                card_search.setVisibility(View.VISIBLE);
                //card_search.startAnimation(animFadeIn);
                toolbar.setVisibility(View.VISIBLE);
            }
        });

        txt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
                r_confirm_logout.setVisibility(View.VISIBLE);
                r_confirm_logout.startAnimation(animFadeIn);
            }
        });
        r_confirm_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_confirm_logout.setVisibility(View.GONE);
                r_confirm_logout.startAnimation(animRollDemande);
            }
        });
        btn_no_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_confirm_logout.setVisibility(View.GONE);
                r_confirm_logout.startAnimation(animRollDemande);
            }
        });
        btn_yes_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                l_confirm_logout.setVisibility(View.GONE);
                l_confirm_logout.startAnimation(animRollDemande);
                r_my_loader.setVisibility(View.VISIBLE);
                new CountDownTimer(1000, 500) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        if (user != null) {
                            if (sessionManagerClient.isLoggedIn()) {
                                sessionManagerClient.clearClientLoginData();
                            }
                            mAuth.signOut();
                            startActivity(new Intent(getApplicationContext(), Login.class));
                            finish();

                        }
                    }
                }.start();
            }
        });
        btn_roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_my_loader.setVisibility(View.VISIBLE);
                r_rooll_course.setVisibility(View.GONE);
                r_search_course.setVisibility(View.GONE);
                final DatabaseReference d_inc = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Clients").child(mAuth.getCurrentUser().getUid());
                d_inc.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String inc = dataSnapshot.child("increment").getValue().toString();
                        final DatabaseReference d_up = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes").child(mAuth.getCurrentUser().getUid() + "_" + inc);
                        d_up.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(final DataSnapshot dataSnapshot) {
                                HashMap<String, Object> res = new HashMap<>();
                                res.put("Etat", "Demande Canceled");
                                if (init_arc == 1) {
                                    init_arc = 0;
                                }
                                if (init_dem_encours == 1) {
                                    init_dem_encours = 0;
                                }
                                d_up.updateChildren(res).addOnCompleteListener(Home.this, new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        r_my_loader.setVisibility(View.GONE);
                                        toolbar.setVisibility(View.VISIBLE);
                                        toolbar.startAnimation(animFadeIn);
                                        card_search.setVisibility(View.VISIBLE);
                                        //card_search.startAnimation(animFadeIn);
                                        mMap.getUiSettings().setAllGesturesEnabled(true);
                                        mMap.getUiSettings().setRotateGesturesEnabled(false);
                                        mMap.getUiSettings().setMapToolbarEnabled(false);
                                        r_rooll_course.setVisibility(View.GONE);
                                        r_search_course.setVisibility(View.GONE);
                                        r_sect_demande_accepted.setVisibility(View.GONE);
                                    }
                                });
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                /*DatabaseReference del = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes").child(mAuth.getCurrentUser().getUid());
                del.removeValue();*/
            }
        });
        /*
        * Confirmer la demande
        * */
        btn_confirm_commande.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                String[] p = now.getTime().toString().split(" ");
                String day = p[2];
                String month = p[1];
                String[] time = p[3].split(":");
                String heure = time[0];
                String minute = time[1];
                String annee = p[5];
                String mois = formatMois(month);
                //
                final String date = day + "/" + mois + "/" + annee;
                final String temps = heure + ":" + minute;
                user_id = mAuth.getCurrentUser().getUid();
                //Incrémenter la valeur de incrément
                final DatabaseReference d_inc = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Clients").child(user_id);
                d_inc.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final int inc = Integer.parseInt(dataSnapshot.child("increment").getValue().toString()) + 1;
                        HashMap<String, Object> res = new HashMap<>();
                        res.put("increment", String.valueOf(inc));
                        d_inc.updateChildren(res).addOnCompleteListener(Home.this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    mMap.clear();
                                    //Procéder à la création de la demande
                                    DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes").child(user_id + "_" + inc);
                                    current_user_db.child("AdresseDepart").setValue(AdresseDepart);
                                    current_user_db.child("GpsDepart").setValue(GpsDepart);
                                    current_user_db.child("AdresseArrivee").setValue(AdresseArrivee);
                                    current_user_db.child("GpsArrivee").setValue(GpsArrivee);
                                    current_user_db.child("Distance").setValue(Distance);
                                    current_user_db.child("Duree").setValue(Duree);
                                    current_user_db.child("Type").setValue("Immédiat");
                                    current_user_db.child("Date").setValue(date);
                                    current_user_db.child("Heure").setValue(temps);
                                    current_user_db.child("Etat").setValue("Nouvelle demande");
                                    current_user_db.child("Lattitude").equals(lat);
                                    current_user_db.child("longitude").equals(lng);

                                    r_sect_confirm_demande.setVisibility(View.GONE);
                                    r_my_loader.setVisibility(View.VISIBLE);
                                    new CountDownTimer(1000, 500) {
                                        @Override
                                        public void onTick(long millisUntilFinished) {

                                        }

                                        @Override
                                        public void onFinish() {
                                            if (init_arc == 0) {
                                                init_arc = 1;
                                            }
                                            r_my_loader.setVisibility(View.GONE);
                                            r_search_course.setVisibility(View.VISIBLE);
                                            mMap.getUiSettings().setAllGesturesEnabled(false);
                                            mMap.getUiSettings().setZoomGesturesEnabled(false);
                                            mMap.getUiSettings().setScrollGesturesEnabled(false);
                                            mMap.getUiSettings().setMapToolbarEnabled(false);
                                            mMap.getUiSettings().setRotateGesturesEnabled(false);
                                        }
                                    }.start();
                                }
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
        /*
        * Fin de la confirmation de la demande
        * */
        r_sect_error_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_sect_error_home.setVisibility(View.GONE);
                r_sect_error_home.startAnimation(animRollDemande);
            }
        });
        /*
        *
        * */
        //Activer le place autocompléte
        txt_my_position_arrive.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String cheq = String.valueOf(s);
                if (!cheq.equals("")) {
                    progress_wait.setVisibility(View.VISIBLE);
                    final OkHttpClient client = new OkHttpClient();
                    RequestBody body = new FormBody.Builder()
                            .add("text", cheq)
                            .build();

                    final Request request = new Request.Builder()
                            .url(URL + cheq + "&types=geocode&location=" + lat + "," + lng + "&radius=20000&strictbounds&language=fr&key=" + KEY)
                            .addHeader("Content-Type", "text/json; Charset=UTF-8")
                            .build();
                    AsyncTask<Void, Void, JSONObject> asyncTask = new AsyncTask<Void, Void, JSONObject>() {
                        @Override
                        protected JSONObject doInBackground(Void... params) {
                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                            StrictMode.setThreadPolicy(policy);
                            Response response = null;
                            JSONObject jsonObject = null;
                            try {
                                response = client.newCall(request).execute();
                                if (!response.isSuccessful()) {
                                    return null;
                                } else {
                                    jsonObject = new JSONObject(response.body().string());
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            return jsonObject;
                        }

                        @Override
                        protected void onPostExecute(JSONObject jsonObject) {
                            super.onPostExecute(jsonObject);
                            try {
                                JSONArray j_prediction = jsonObject.getJSONArray("predictions");
                                Log.d(TAG, "Reponse: " + j_prediction);
                                Log.d(TAG, "nombre: " + j_prediction.length());
                                if (j_prediction.length() != 0) {
                                    JSONObject j_section = null;
                                    localities.clear();
                                    for (int i = 0; i < j_prediction.length(); i++) {
                                        j_section = j_prediction.getJSONObject(i);
                                        String[] p = j_section.optString("description").split(",");
                                        JSONArray j_terms = j_section.getJSONArray("terms");
                                        JSONObject j_ville = (JSONObject) j_terms.get(1);
                                        JSONObject j_pays = (JSONObject) j_terms.get(2);
                                        Log.d(TAG, "Terms: " + j_terms);
                                        Log.d(TAG, "Ville: " + j_ville);
                                        Log.d(TAG, "Pays: " + j_pays);
                                        Log.d(TAG, "Description: " + j_section.optString("description"));
                                        Log.d(TAG, "id: " + j_section.optString("id"));
                                        if (j_section != null && j_ville != null && j_pays != null) {
                                            list_adresse.setVisibility(View.VISIBLE);
                                            txt_nothing_adress.setVisibility(View.GONE);
                                            locality = new Locality();
                                            locality.setId(j_section.optString("id"));
                                            locality.setAdresse(p[0]);
                                            locality.setVille(j_ville.optString("value"));
                                            locality.setPays(j_pays.optString("value"));
                                            localities.add(locality);
                                            adapter = new Locality_Adapter(getApplicationContext(), localities);
                                            adapter.notifyDataSetChanged();
                                            list_adresse.setAdapter(adapter);
                                            progress_wait.setVisibility(View.GONE);
                                            setListViewHeightBasedOnChildren(list_adresse);
                                        }
                                    }
                                } else {
                                    progress_wait.setVisibility(View.GONE);
                                    txt_nothing_adress.setVisibility(View.VISIBLE);
                                    list_adresse.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    asyncTask.execute();
                } else {
                    progress_wait.setVisibility(View.GONE);
                    txt_nothing_adress.setVisibility(View.VISIBLE);
                    list_adresse.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        list_adresse.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Locality locality = localities.get(position);
                if (locality.getId() != null) {
                    r_section_search.setVisibility(View.GONE);
                    r_my_loader.setVisibility(View.VISIBLE);
                    card_search.setVisibility(View.GONE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(txt_my_position_arrive, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    String dep = lat + "," + lng;
                    String arrive = locality.getAdresse() + "," + ville;
                    AdresseArrivee = locality.getAdresse();
                    sendRequest(dep, arrive);
                }
            }
        });
    }

    /*===========================================================================*/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(this, data);
            if (place == null) {
                Log.i(TAG, "No place selected");
                return;
            }

            String placeID = place.getId();
            txt_my_position_arrive.setText(place.getName());

            // Insert a new place into DB
            ContentValues contentValues = new ContentValues();
            contentValues.put(PlaceContract.PlaceEntry.COLUMN_PLACE_ID, placeID);
            //getContentResolver().insert(PlaceContract.PlaceEntry.CONTENT_URI, contentValues);

            // Get live data information
            refreshPlacesData();
        }
    }

    public void refreshPlacesData() {
        Uri uri = PlaceContract.PlaceEntry.CONTENT_URI;
        Cursor data = getContentResolver().query(
                uri,
                null,
                null,
                null,
                null);

        if (data == null || data.getCount() == 0) return;
        List<String> guids = new ArrayList<String>();
        while (data.moveToNext()) {
            guids.add(data.getString(data.getColumnIndex(PlaceContract.PlaceEntry.COLUMN_PLACE_ID)));
        }
        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mClient,
                guids.toArray(new String[guids.size()]));
        placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
            @Override
            public void onResult(@NonNull PlaceBuffer places) {
                mAdapter.swapPlaces(places);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        init_map = true;
        GoogleDirectionConfiguration.getInstance().setLogEnabled(true);

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json);
        mMap.setMapStyle(style);

        if (isNetworkAvailable(Home.this)) {
            if (mGPS.canGetLocation) {
                mGPS.getLocation();
                lattitude = mGPS.getLatitude();
                longitude = mGPS.getLongitude();
            }
        }
        pos_custumer = new LatLng(lattitude, longitude);
        LatLng latEnd = new LatLng(31.671630, -8.025708);
        /*======================================================*/
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> address = null;
        try {
            address = geocoder.getFromLocation(lattitude, longitude, 1);
            Log.d(TAG, "Adresse: " + address);
            if (address != null) {
                Address fetAddress1 = address.get(0);
                StringBuilder strAdress = new StringBuilder();
                for (int i = 0; i < fetAddress1.getMaxAddressLineIndex(); i++) {
                    strAdress.append(fetAddress1.getAddressLine(i)).append("\n");
                }
                ville = fetAddress1.getLocality();
                //if (!fetAddress1.getAddressLine(0).equals("Unnamed Road")) {
                txt_my_position.setText(fetAddress1.getAddressLine(0));
                // }
                Log.d(TAG, "Informations sur l'adresse:" + fetAddress1.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*======================================================*/

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos_custumer, 14));
        //mMap.getUiSettings().setCompassEnabled(false);
        // mMap.getUiSettings().setTiltGesturesEnabled(false);
        // mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setBuildingsEnabled(true);
        mMap.setMinZoomPreference(6.0f);
        mMap.setMaxZoomPreference(18.0f);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        //mMap.setOnCameraMoveListener(this);

        /*mapRipple = new MapRipple(mMap, pos_custumer, getApplicationContext());
        mapRipple.withStrokeColor(Color.parseColor("#F3BE16"));
        mapRipple.withStrokewidth(12);
        mapRipple.withDistance(2000) ;
        mapRipple.withRippleDuration(8000);
        mapRipple.withTransparency(0.5f);
        mapRipple.startRippleMapAnimation();*/
    }


    @Override
    public void onLocationChanged(Location location) {
        if (mGPS.canGetLocation) {
            mGPS.getLocation();
            lattitude = mGPS.getLatitude();
            longitude = mGPS.getLongitude();
            Toast.makeText(getApplicationContext(), String.valueOf(lattitude), Toast.LENGTH_LONG).show();
        }
        GeomagneticField field = new GeomagneticField(
                (float) location.getLatitude(),
                (float) location.getLongitude(),
                (float) location.getAltitude(),
                System.currentTimeMillis()
        );
        altitude = (float) location.getAltitude();

        // getDeclination returns degrees
        mDeclination = field.getDeclination();
        Log.d(TAG, "Lattitude :" + lattitude);
    }


    public void animateMarker(final GoogleMap map, final Marker marker, final LatLng toPosition) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 6000;
        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * toPosition.latitude + (1 - t) * startLatLng.longitude;
                double lat = t * toPosition.longitude + (1 - t) * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                if (t < 1.0) {
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    private static float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }


    private Location convertLatLngToLocation(LatLng latLng) {
        Location location = new Location("someLoc");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }

    private float bearingBetweenLatLngs(LatLng beginLatLng, LatLng endLatLng) {
        Location beginLocation = convertLatLngToLocation(beginLatLng);
        Location endLocation = convertLatLngToLocation(endLatLng);
        return beginLocation.bearingTo(endLocation);
    }

    private void updatePolyLine(LatLng currentlatlng) {
        polylinePaths.remove();
        if (originMarkers.isVisible()) {
            originMarkers.remove();
        }
        //mMap.clear();
        if (mGPS.canGetLocation) {
            mGPS.getLocation();
            lattitude = mGPS.getLatitude();
            longitude = mGPS.getLongitude();
            sendRequest(lattitude + "," + longitude, "Résidence Tachefine Marrakech");
        }
        myHandler = new Handler();
        myHandler.postDelayed(myRunPolyline, 3000);
    }

    private void updateMarkerPosition(Location newLocation) {

        LatLng newLatLng = new LatLng(newLocation.getLatitude(), newLocation.getLongitude());

        if (ourGlobalMarker == null) { // First time adding marker to map
            ourGlobalMarker = mMap.addMarker(new MarkerOptions().position(newLatLng));
        } else {
            //MarkerAnimation.animateMarkerToICS(ourGlobalMarker, newLatLng, new LatLngInterpolator.Spherical());
        }
    }

    //Initialisation de mon animation sur la section search
    private void initAnimation() {
        animSideUp = AnimationUtils.loadAnimation(this, R.anim.pump_top);
        animSideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void rollAnimation() {
        animSideRoll = AnimationUtils.loadAnimation(this, R.anim.pump_bottom);
        animSideRoll.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void SlideUpAnimation() {
        animSlideUp = AnimationUtils.loadAnimation(this, R.anim.grow_from_bottom);
        animSlideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void SlideDownAnimation() {
        animSlideDown = AnimationUtils.loadAnimation(this, R.anim.disappear);
        animSlideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void RollDemandeAnimation() {
        animRollDemande = AnimationUtils.loadAnimation(this, R.anim.disappear);
        animRollDemande.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /*======================================================*/
    //Mes animation
    private void fadeOutAnime() {
        animSideFadeOut = AnimationUtils.loadAnimation(this, R.anim.outgoing);
        animSideFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                r_home.setVisibility(View.GONE);
                startActivity(intent);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void fadeinAnime() {
        animFadeIn = AnimationUtils.loadAnimation(this, R.anim.incoming);
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void sendRequest(String depart, String arrivee) {
        try {
            new DirectionFinder((DirectionFinderListener) Home.this, depart, arrivee).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDirectionFinderStart() {
        if (polylinePaths != null) {
            polylinePaths.remove();
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        //polylinePaths = new ArrayList<>();

        if (!routes.isEmpty()) {
            r_my_loader.setVisibility(View.GONE);
            init_polyline = 1;
            Log.d(TAG, "routes: " + routes);
            for (Route route : routes) {

                //Calculer le centre du plyline
                int nbrpoint = route.points.size();
                float modulopoint = nbrpoint / 2;
                String[] ps = String.valueOf(modulopoint).split("\\.");
                int mypoint = Integer.parseInt(ps[0]);
                position_driver = new LatLng(route.points.get(mypoint).latitude, route.points.get(mypoint).longitude);
                if (route.distance.value <= 200) {
                    if (zoom0 == 0) {
                        zoom0 = 1;
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position_driver, 18));
                    }
                } else if (route.distance.value > 200 && route.distance.value <= 500) {
                    if (zoom1 == 0) {
                        zoom1 = 1;
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position_driver, 17));
                    }
                } else if (route.distance.value > 500 && route.distance.value <= 1000) {
                    if (zoom2 == 0) {
                        zoom2 = 1;
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position_driver, 16));
                    }
                } else if (route.distance.value > 1000 && route.distance.value <= 2000) {
                    if (zoom3 == 0) {
                        zoom3 = 1;
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position_driver, 15));
                    }
                } else if (route.distance.value > 2000 && route.distance.value <= 3000) {
                    if (zoom4 == 0) {
                        zoom4 = 1;
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position_driver, 14));
                    }
                } else if (route.distance.value > 3000 && route.distance.value <= 4000) {
                    if (zoom5 == 0) {
                        zoom5 = 1;
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position_driver, 13));
                    }
                } else {
                    if (zoom6 == 0) {
                        zoom6 = 1;
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position_driver, 12));
                    }
                }
                /*===================================================================*/
                AdresseDepart = route.startAddress;
                GpsDepart = route.startLocation.latitude + "," + route.startLocation.longitude;
                //AdresseArrivee = route.endAddress;
                GpsArrivee = route.endLocation.latitude + "," + route.endLocation.longitude;
                Distance = route.distance.text;
                Duree = route.duration.text;
                /*===================================================================*/
                originMarkers = mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.pin_depart)).
                                position(route.startLocation).flat(true).anchor((float) 0.5, (float) 0.5).rotation((float) 40.0));
                destinationMarkers = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.pin_destination)).position(route.endLocation).flat(true).anchor((float) 0.5, (float) 0.5).rotation((float) 40.0));
                polylineOptions = new PolylineOptions().
                        geodesic(true).
                        color(Color.parseColor("#F3BE16")).
                        width(6);
                Log.d(TAG, "Les points du trajet: " + route.points.toString());
                for (int i = 0; i < route.points.size(); i++) {
                    if (init_arc == 1) {
                        polylineOptions.add(route.points.get(i));
                    }
                    pitrajet = new Points(route.points.get(i).latitude, route.points.get(i).longitude);
                    mayTatlngs.add(pitrajet);
                }
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(false);
                if (init_arc == 0) {
                    double cLat = ((route.startLocation.latitude + route.endLocation.latitude) / 2);
                    double cLon = ((route.startLocation.longitude + route.endLocation.longitude) / 2);

                    //add skew and arcHeight to move the midPoint
                    if (Math.abs(route.startLocation.longitude - route.endLocation.longitude) < 0.0001) {
                        cLon -= 0.0195;
                    } else {
                        cLat += 0.0195;
                    }

                    List<LatLng> alLatlng = new ArrayList<>();
                    double tDelta = 1.0 / 50;
                    for (double t = 0; t <= 1.0; t += tDelta) {
                        double oneMinusT = (1.0 - t);
                        double t2 = Math.pow(t, 2);
                        double lon = oneMinusT * oneMinusT * route.startLocation.longitude
                                + 2 * oneMinusT * t * cLon
                                + t2 * route.endLocation.longitude;
                        double lat = oneMinusT * oneMinusT * route.startLocation.latitude
                                + 2 * oneMinusT * t * cLat
                                + t2 * route.endLocation.latitude;
                        alLatlng.add(new LatLng(lat, lon));
                    }
                    polylineOptions.addAll(alLatlng);
                    r_sect_validate_demande.setVisibility(View.VISIBLE);
                    r_sect_validate_demande.startAnimation(animFadeIn);
                }
                polylinePaths = mMap.addPolyline(polylineOptions);
            }
        }else {
            r_my_loader.setVisibility(View.GONE);
            card_search.setVisibility(View.VISIBLE);
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Home.this);
            alertDialogBuilder.setTitle("Erreur de la recherche");
            alertDialogBuilder.setMessage("Aucune route n'a été trouvée pour cette destination.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    //Ecouteur de la direction

    private Runnable myRunPolyline = new Runnable() {
        @Override
        public void run() {
            if (isNetworkAvailable(Home.this)) {
                if (mGPS.canGetLocation) {
                    mGPS.getLocation();
                    lattitude = mGPS.getLatitude();
                    longitude = mGPS.getLongitude();
                }
                /*
                * Vérifier l'existance d'une demande
                * */
                final DatabaseReference d_inc = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Clients").child(mAuth.getCurrentUser().getUid());
                d_inc.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        inc = dataSnapshot.child("increment").getValue().toString();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                new_demande = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes").child(mAuth.getCurrentUser().getUid()+"_"+inc);
                new_demande.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            etat = dataSnapshot.getValue().toString();
                            Key_dem = dataSnapshot.getKey();
                            if (!etat.equals("Demande closed")) {
                                if (etat.equals("Nouvelle demande")) {
                                    status_dem = "nouvelle";
                                    init_arc = 1;
                                    init_dem_encours = 1;
                                    toolbar.setVisibility(View.GONE);
                                    card_search.setVisibility(View.GONE);
                                    r_my_loader.setVisibility(View.GONE);
                                    r_search_course.setVisibility(View.VISIBLE);
                                    mMap.getUiSettings().setAllGesturesEnabled(false);
                                    mMap.getUiSettings().setZoomGesturesEnabled(false);
                                    mMap.getUiSettings().setScrollGesturesEnabled(false);
                                    mMap.getUiSettings().setMapToolbarEnabled(false);
                                    mMap.getUiSettings().setRotateGesturesEnabled(false);
                                    //
                                    txt_adresse_dep_demande.setText(dataSnapshot.child("AdresseDepart").getValue().toString());
                                    txt_adresse_arrv_demande.setText(dataSnapshot.child("AdresseArrivee").getValue().toString());

                                    if (isNetworkAvailable(Home.this)) {
                                        if (mGPS.canGetLocation) {
                                            mGPS.getLocation();
                                            lattitude = mGPS.getLatitude();
                                            longitude = mGPS.getLongitude();

                                            HashMap<String, Object> resPosition = new HashMap<>();
                                            resPosition.put("Lattitude", lattitude);
                                            resPosition.put("Longitude", longitude);
                                            new_demande.updateChildren(resPosition);
                                        }
                                    }
                                }
                                if (etat.equals("Demande accepted")) {
                                    init_arc = 1;
                                    if (init_loader == 0) {
                                        final DatabaseReference dt_course = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Courses").child(mAuth.getCurrentUser().getUid() + "_" + inc);
                                        if (dt_course.getKey() != null) {
                                            dt_course.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    try {
                                                        String ch_id = dataSnapshot.child("chauffeur_id").getValue().toString();
                                                        final DatabaseReference dt_chauffeur = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Chauffeurs").child(ch_id);
                                                        dt_chauffeur.addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                                txt_etoile_driver_accepted.setText(dataSnapshot.child("etoile").getValue().toString());
                                                                txt_name_driver_accepted.setText(dataSnapshot.child("prenom").getValue().toString() + " " + dataSnapshot.child("nom").getValue().toString());
                                                                tel_driver = dataSnapshot.child("tel").getValue().toString();
                                                                name_driver_detail.setText(dataSnapshot.child("prenom").getValue().toString() + " " + dataSnapshot.child("nom").getValue().toString());
                                                                score_chauffeur.setEnabled(false);
                                                                score_chauffeur.setRating(Float.parseFloat(dataSnapshot.child("etoile").getValue().toString()));
                                                                score_chauffeur_arrived.setEnabled(false);
                                                                score_chauffeur_arrived.setRating(Float.parseFloat(dataSnapshot.child("etoile").getValue().toString()));
                                                                txt_name_driver_arrived.setText(dataSnapshot.child("prenom").getValue().toString() + " " + dataSnapshot.child("nom").getValue().toString());
                                                            }

                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {

                                                            }
                                                        });
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    //sendRequest(gpsDepart,getGpsArrivee);
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                        myHandlerCours = new Handler();
                                        myHandlerCours.postDelayed(myRunMyPoliline, 5);
                                        init_loader = 1;
                                        init_dem_encours = 1;
                                        //r_my_loader.setVisibility(View.VISIBLE);
                                        gpsDepart = dataSnapshot.child("GpsDepart").getValue().toString();
                                        getGpsArrivee = dataSnapshot.child("GpsArrivee").getValue().toString();
                                        init_polyline = 0;
                                        r_my_loader.setVisibility(View.GONE);
                                        r_search_course.setVisibility(View.GONE);
                                        r_sect_demande_accepted.setVisibility(View.VISIBLE);
                                        r_sect_validate_demande.setVisibility(View.GONE);
                                        toolbar.setVisibility(View.GONE);
                                        card_search.setVisibility(View.GONE);
                                        mMap.getUiSettings().setMapToolbarEnabled(false);
                                        mMap.getUiSettings().setRotateGesturesEnabled(false);
                                        mMap.getUiSettings().setAllGesturesEnabled(true);
                                        mMap.setBuildingsEnabled(true);
                                        mMap.setMinZoomPreference(6.0f);
                                        mMap.setMaxZoomPreference(18.0f);
                                        if (isNetworkAvailable(Home.this)) {
                                            if (mGPS.canGetLocation) {
                                                mGPS.getLocation();
                                                lattitude = mGPS.getLatitude();
                                                longitude = mGPS.getLongitude();

                                                HashMap<String, Object> resPosition = new HashMap<>();
                                                resPosition.put("Lattitude", lattitude);
                                                resPosition.put("Longitude", longitude);
                                                new_demande.updateChildren(resPosition);
                                            }
                                        }
                                    }
                                }
                                if (etat.equals("Driver arrived")) {
                                    init_arc = 1;
                                    if (init_loader == 0) {
                                        myHandlerCours = new Handler();
                                        myHandlerCours.postDelayed(myRunMyPoliline, 5);
                                        init_loader = 1;
                                        init_dem_encours = 1;
                                        init_polyline = 0;
                                        r_my_loader.setVisibility(View.GONE);
                                        r_search_course.setVisibility(View.GONE);
                                        r_sect_demande_accepted.setVisibility(View.GONE);
                                        r_sect_validate_demande.setVisibility(View.GONE);
                                        r_sect_driver_arrived.setVisibility(View.VISIBLE);
                                        toolbar.setVisibility(View.GONE);
                                        card_search.setVisibility(View.GONE);
                                        mMap.getUiSettings().setMapToolbarEnabled(false);
                                        mMap.getUiSettings().setRotateGesturesEnabled(false);
                                        mMap.getUiSettings().setAllGesturesEnabled(true);
                                        mMap.setBuildingsEnabled(true);
                                        mMap.setMinZoomPreference(6.0f);
                                        mMap.setMaxZoomPreference(18.0f);
                                        txt_adresse_arrv_demande_arrived.setText(dataSnapshot.child("AdresseArrivee").getValue().toString());
                                        xt_adresse_dep_demande_arrived.setText(dataSnapshot.child("AdresseDepart").getValue().toString());
                                    }
                                }
                                if (etat.equals("Custumer at board")) {
                                    init_arc = 1;
                                    if (init_loader == 0) {
                                        myHandlerCours = new Handler();
                                        myHandlerCours.postDelayed(myRunMyPoliline, 5);
                                        init_loader = 1;
                                        init_dem_encours = 1;
                                        init_polyline = 0;
                                        r_my_loader.setVisibility(View.GONE);
                                        r_search_course.setVisibility(View.GONE);
                                        r_sect_demande_accepted.setVisibility(View.GONE);
                                        r_sect_validate_demande.setVisibility(View.GONE);
                                        r_sect_driver_arrived.setVisibility(View.VISIBLE);
                                        l_sect_cancel_ride.setVisibility(View.GONE);
                                        toolbar.setVisibility(View.GONE);
                                        card_search.setVisibility(View.GONE);
                                        mMap.getUiSettings().setMapToolbarEnabled(false);
                                        mMap.getUiSettings().setRotateGesturesEnabled(false);
                                        mMap.getUiSettings().setAllGesturesEnabled(true);
                                        mMap.setBuildingsEnabled(true);
                                        mMap.setMinZoomPreference(6.0f);
                                        mMap.setMaxZoomPreference(18.0f);
                                        txt_adresse_arrv_demande_arrived.setText(dataSnapshot.child("AdresseArrivee").getValue().toString());
                                        xt_adresse_dep_demande_arrived.setText(dataSnapshot.child("AdresseDepart").getValue().toString());
                                    }
                                }
                                if (etat.equals("Custumer deposed")) {
                                    init_arc = 1;
                                    if (init_loader == 0) {
                                        init_loader = 1;
                                        init_dem_encours = 1;
                                        init_polyline = 0;
                                        r_rate_course.setVisibility(View.VISIBLE);
                                        r_my_loader.setVisibility(View.GONE);
                                        r_search_course.setVisibility(View.GONE);
                                        r_sect_demande_accepted.setVisibility(View.GONE);
                                        r_sect_validate_demande.setVisibility(View.GONE);
                                        r_sect_driver_arrived.setVisibility(View.GONE);
                                        l_sect_cancel_ride.setVisibility(View.GONE);
                                        toolbar.setVisibility(View.GONE);
                                        card_search.setVisibility(View.GONE);
                                        mMap.clear();
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                //
                /*=================================================================================*/
        /*=================================================================================*/
                myHandler.postDelayed(this,1000);
            }else {
                toolbar.setVisibility(View.GONE);
                card_search.setVisibility(View.GONE);
                r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                r_sect_not_internet_connexion.startAnimation(animFadeIn);
            }
        }
    };
    private Runnable myRunMyPoliline = new Runnable() {
        @Override
        public void run() {
            final DatabaseReference dt_course = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Courses").child(mAuth.getCurrentUser().getUid()+"_"+inc);
            if (dt_course.getKey() != null){
                dt_course.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            String ch_id = dataSnapshot.child("chauffeur_id").getValue().toString();
                            final DatabaseReference dt_chauffeur = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Chauffeurs").child(ch_id);
                            dt_chauffeur.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    txt_etoile_driver_accepted.setText(dataSnapshot.child("etoile").getValue().toString());
                                    txt_name_driver_accepted.setText(dataSnapshot.child("prenom").getValue().toString() + " " + dataSnapshot.child("nom").getValue().toString());
                                    tel_driver = dataSnapshot.child("tel").getValue().toString();
                                    name_driver_detail.setText(dataSnapshot.child("prenom").getValue().toString() + " " + dataSnapshot.child("nom").getValue().toString());
                                    score_chauffeur.setEnabled(false);
                                    score_chauffeur.setRating(Float.parseFloat(dataSnapshot.child("etoile").getValue().toString()));
                                    score_chauffeur_arrived.setEnabled(false);
                                    score_chauffeur_arrived.setRating(Float.parseFloat(dataSnapshot.child("etoile").getValue().toString()));
                                    txt_name_driver_arrived.setText(dataSnapshot.child("prenom").getValue().toString() + " " + dataSnapshot.child("nom").getValue().toString());
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        //sendRequest(gpsDepart,getGpsArrivee);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
            myHandlerCours.postDelayed(this,20000);
        }
    };
    /*======================================================*/
    public static boolean isNetworkAvailable(Context context)
    {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthStateListener);
        myHandler = new Handler();
        myHandler.postDelayed(myRunPolyline, 2);
        /*if (mapRipple.isAnimationRunning() ) {
            mapRipple.startRippleMapAnimation();
        }*/
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthStateListener);
        if(myHandler != null) {
            myHandler.removeCallbacks(myRunPolyline);
        }
        /*if (mapRipple.isAnimationRunning() != false) {
            mapRipple.stopRippleMapAnimation();
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(myHandler != null) {
            myHandler.removeCallbacks(myRunPolyline);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (status_dem.equals("nouvelle")){
                final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Home.this);
                alertDialogBuilder.setMessage("Voulez-vous quitter l'application?");

                alertDialogBuilder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.hide();
                        finish();
                    }
                });
                alertDialogBuilder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.hide();
                    }
                });

                android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }else {
                if (init_polyline == 1) {
                    if (init_dem_encours == 0) {
                        mMap.clear();
                        init_polyline = 0;
                        niv_back = 0;
                        card_search.setVisibility(View.VISIBLE);
                        //card_search.startAnimation(animFadeIn);
                        toolbar.setVisibility(View.VISIBLE);
                        //toolbar.startAnimation(animSideUp);
                        r_sect_confirm_demande.setVisibility(View.GONE);
                        r_sect_validate_demande.setVisibility(View.GONE);
                    } else {
                        if (niv_back == 0) {
                            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Home.this);
                            alertDialogBuilder.setMessage("Voulez-vous quitter l'application?");

                            alertDialogBuilder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.hide();
                                    finish();
                                }
                            });
                            alertDialogBuilder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.hide();
                                }
                            });

                            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                        r_sect_contact_driver.setVisibility(View.GONE);
                    }
                } else {
                    if (niv_back == 1) {
                        niv_back = 0;
                        if (init_detail_chauffeur == 0) {
                            r_sect_add_adress.setVisibility(View.GONE);
                            r_section_search.setVisibility(View.GONE);
                            card_search.setVisibility(View.VISIBLE);
                            //r_section_search.startAnimation(animSideRoll);
                            toolbar.setVisibility(View.VISIBLE);
                            toolbar.startAnimation(animSideUp);
                            r_section_adresse.setVisibility(View.GONE);
                            r_section_adresse.startAnimation(animSlideDown);
                        }else {
                            init_detail_chauffeur = 0;
                            r_sect_detail_driver.setVisibility(View.GONE);
                        }
                    } else {
                        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Home.this);
                        alertDialogBuilder.setMessage("Voulez-vous quitter l'application?");

                        alertDialogBuilder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.hide();
                                finish();
                            }
                        });
                        alertDialogBuilder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.hide();
                            }
                        });

                        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                }

            }
        }
        return false;
    }
    //Mes méthodes
    private String formatMois(String month){
        String mois = "";
        if (month.equals("Jan")){
            mois = "01";
        }else if (month.equals("Fev")){
            mois = "02";
        }else if (month.equals("Mar")){
            mois = "03";
        }else if (month.equals("Apr")){
            mois = "04";
        }else if (month.equals("May")){
            mois = "05";
        }else if (month.equals("Jun")){
            mois = "06";
        }else if (month.equals("Jul")){
            mois = "07";
        }else if (month.equals("Aug")){
            mois = "08";
        }else if (month.equals("Sep")){
            mois = "09";
        }else if (month.equals("Oct")){
            mois = "10";
        }else if (month.equals("Nov")){
            mois = "11";
        }else if (month.equals("Dec")){
            mois = "12";
        }
        return mois;
    }
    private Clients get_client(String tel){
        Clients cl = new Clients();
        String result = "";
        OkHttpClient client = new OkHttpClient();

        RequestBody body = new FormBody.Builder()
                .add("tel",tel)
                .add("token",token)
                .build();

        Request request = new Request.Builder()
                .url(lien+"clients/get_client_by_telephone.php")
                .post(body)
                .build();
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Response response = client.newCall(request).execute();
            result = response.body().string();
            Log.d(TAG,"Client :"+result);
            if (result != null){
                JSONObject jsonObject = new JSONObject(result);
                cl.setId(jsonObject.optInt("id"));
                cl.setTel(jsonObject.optString("telephone"));
                cl.setNom(jsonObject.optString("nom"));
                cl.setPrenom(jsonObject.optString("prenom"));
                cl.setDateCreation(jsonObject.optString("dateNaissance"));
                cl.setEmail(jsonObject.optString("email"));
                cl.setPhoto(jsonObject.optString("photo"));
                cl.setStatut(jsonObject.optInt("statut"));
                cl.setEtat(jsonObject.optString("etat"));
                cl.setAdresse(jsonObject.optString("adresse"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cl;
    }
    //La méthode qui permet de déconnecter l'utilisateur
    private String logout_client(String email){
        Clients cl = new Clients();
        String result = "";
        OkHttpClient client = new OkHttpClient();

        RequestBody body = new FormBody.Builder()
                .add("email",email)
                .add("token",token)
                .build();

        Request request = new Request.Builder()
                .url(lien+"clients/logout_client.php")
                .post(body)
                .build();

        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Response response = client.newCall(request).execute();
            result = response.body().string();
            Log.d(TAG,"statut déconnexion :"+result);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    //Les directions
    public void handleGetDirectionsResult(ArrayList<LatLng> directionPoints)
    {
        Polyline newPolyline;
        PolylineOptions rectLine = new PolylineOptions().width(3).color(Color.RED);

        for(int i = 0 ; i < directionPoints.size() ; i++)
        {
            rectLine.add(directionPoints.get(i));
        }
        newPolyline = mMap.addPolyline(rectLine);
    }

    public void findDirections(double fromPositionDoubleLat, double fromPositionDoubleLong, double toPositionDoubleLat, double toPositionDoubleLong, String mode)
    {
        Map<String, String> map = new HashMap<String, String>();
        map.put(GetDirectionsAsyncTask.USER_CURRENT_LAT, String.valueOf(fromPositionDoubleLat));
        map.put(GetDirectionsAsyncTask.USER_CURRENT_LONG, String.valueOf(fromPositionDoubleLong));
        map.put(GetDirectionsAsyncTask.DESTINATION_LAT, String.valueOf(toPositionDoubleLat));
        map.put(GetDirectionsAsyncTask.DESTINATION_LONG, String.valueOf(toPositionDoubleLong));
        map.put(GetDirectionsAsyncTask.DIRECTIONS_MODE, mode);

        GetDirectionsAsyncTask asyncTask = new GetDirectionsAsyncTask(this);
        asyncTask.execute(map);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            SensorManager.getRotationMatrixFromVector(
                    mRotationMatrix , event.values);
            float[] orientation = new float[3];
            SensorManager.getOrientation(mRotationMatrix, orientation);
            float bearing = (float) (Math.toDegrees(orientation[0]) + mDeclination);
            updateCamera(bearing);
        }
    }

    private void updateCamera(float bearing) {
        CameraPosition oldPos = mMap.getCameraPosition();

        CameraPosition pos = CameraPosition.builder(oldPos).zoom(20).bearing(bearing).build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent it = new Intent(Intent.ACTION_CALL);
                    it.setData(Uri.parse("tel:" + tel_driver));
                    if (ActivityCompat.checkSelfPermission(Home.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    r_sect_contact_driver.setVisibility(View.GONE);
                    startActivity(it);
                }else {

                }
            }
        }
    }
}
