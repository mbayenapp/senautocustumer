package com.mbayenn.senauto.Activity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mbayenn.senauto.R;

public class Register extends AppCompatActivity {
    private Animation animFadeIn,animSideFadeOut;
    private RelativeLayout r_register;
    private Toolbar toolbar;
    private Intent intent;
    private EditText txt_nom,txt_prenom,txt_email,txt_password;
    private Button btn_register;
    private RelativeLayout r_my_loader,r_sect_not_internet_connexion;
    private TextView txt_verify_all_champ;
    /*==========================================================*/
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    /*==========================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        /*======================================================*/
        fadeinAnime();;
        fadeOutAnime();
        /*======================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_register);
        toolbar.setTitle("Inscription");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        r_register = (RelativeLayout)findViewById(R.id.r_register);
        r_register.startAnimation(animFadeIn);
        /*======================================================*/
        txt_nom = (EditText)findViewById(R.id.txt_nom);
        txt_prenom = (EditText)findViewById(R.id.txt_prenom);
        txt_email = (EditText)findViewById(R.id.txt_email);
        txt_password = (EditText)findViewById(R.id.txt_password);
        btn_register = (Button)findViewById(R.id.btn_validate_register);
        r_my_loader = (RelativeLayout)findViewById(R.id.r_my_loader);
        r_sect_not_internet_connexion = (RelativeLayout)findViewById(R.id.r_sect_not_internet_connexion);
        txt_verify_all_champ = (TextView)findViewById(R.id.txt_verify_all_champ);
        /*======================================================*/
        mAuth = FirebaseAuth.getInstance();
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null){
                    r_my_loader.setVisibility(View.VISIBLE);
                    new CountDownTimer(1500,500){
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            intent = new Intent(getApplicationContext(),Login.class);
                            startActivity(intent);
                            finish();
                            return;
                        }
                    }.start();
                }
            }
        };
        /*======================================================*/
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String nom = txt_nom.getText().toString().trim();
                final String prenom = txt_prenom.getText().toString().trim();
                final String email = txt_email.getText().toString().trim();
                String password = txt_password.getText().toString().trim();
                if (nom.equals("") || prenom.equals("") || email.equals("") || password.equals("")){
                    r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                    txt_verify_all_champ.setVisibility(View.VISIBLE);
                    txt_verify_all_champ.setText("Vous devez remplir tous les champs!");
                    new CountDownTimer(4000,1000){
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            r_sect_not_internet_connexion.setVisibility(View.GONE);
                            txt_verify_all_champ.setVisibility(View.GONE);
                        }
                    }.start();
                }else {
                    final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                    if (!email.matches(emailPattern)){
                        r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                        txt_verify_all_champ.setVisibility(View.VISIBLE);
                        txt_verify_all_champ.setText("L'adresse email saisie n'est pas valide!");
                        new CountDownTimer(4000,1000){
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                r_sect_not_internet_connexion.setVisibility(View.GONE);
                                txt_verify_all_champ.setVisibility(View.GONE);
                            }
                        }.start();
                    }else {
                        if (password.length() < 6){
                            r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                            txt_verify_all_champ.setVisibility(View.VISIBLE);
                            txt_verify_all_champ.setText("Le mot de passe indiqué est trop court!");
                            new CountDownTimer(4000,1000){
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    r_sect_not_internet_connexion.setVisibility(View.GONE);
                                    txt_verify_all_champ.setVisibility(View.GONE);
                                }
                            }.start();
                        }else {
                            r_my_loader.setVisibility(View.VISIBLE);
                            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    r_my_loader.setVisibility(View.GONE);
                                    if (!task.isSuccessful()){
                                        r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                                        txt_verify_all_champ.setVisibility(View.VISIBLE);
                                        txt_verify_all_champ.setText("Une erreur est survenue lors de la création du compte!");
                                        new CountDownTimer(4000,1000){
                                            @Override
                                            public void onTick(long millisUntilFinished) {

                                            }

                                            @Override
                                            public void onFinish() {
                                                r_sect_not_internet_connexion.setVisibility(View.GONE);
                                                txt_verify_all_champ.setVisibility(View.GONE);
                                            }
                                        }.start();
                                    }else{
                                        String user_id = mAuth.getCurrentUser().getUid();
                                        DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Clients").child(user_id);
                                        current_user_db.setValue(true);
                                        current_user_db.child("prenom").setValue(prenom);
                                        current_user_db.child("nom").setValue(nom);
                                        current_user_db.child("increment").setValue("0");
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            r_register.startAnimation(animSideFadeOut);
            intent = new Intent(getApplicationContext(),Login.class);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            r_register.startAnimation(animSideFadeOut);
            intent = new Intent(getApplicationContext(),Login.class);
        }
        return false;
    }
    /*======================================================*/
    //Mes animation
    private void fadeinAnime(){
        animFadeIn = AnimationUtils.loadAnimation(this,R.anim.incoming);
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void fadeOutAnime(){
        animSideFadeOut = AnimationUtils.loadAnimation(this,R.anim.push_left_out);
        animSideFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                r_register.setVisibility(View.GONE);
                startActivity(intent);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    /*======================================================*/

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthStateListener);
    }
}
