package com.mbayenn.senauto.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mbayenn.senauto.Helpers.SessionManagerClient;
import com.mbayenn.senauto.Model.Clients;
import com.mbayenn.senauto.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login extends AppCompatActivity {
    private static final String TAG = Home.class.getSimpleName();
    private final String lien = "http://mapi.taxim.ma/taxi_api/";
    private final String token = "XETaxi2017";
    private Button btn_login,btn_register;
    private Animation animSideFadeOut,animFadeIn,animHide;
    private LinearLayout l_login,l_sect_show_message;
    private Intent intent;
    private TextView txt_got_to_forgot,txt_verify_connexion,
            txt_verify_all_champ;
    private EditText txt_email,txt_password;
    private SessionManagerClient sessionManagerClient;
    private RelativeLayout r_sect_not_internet_connexion,r_my_loader;
    private ProgressBar prog_reload;
    private int niv_back = 0;
    /*==================================================*/
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseUser user;
    /*==================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        /*==================================================*/
        fadeOutAnime();
        fadeinAnime();
        fadeOutAnimeHide();
        sessionManagerClient = new SessionManagerClient(this);
        /*==================================================*/
        mAuth = FirebaseAuth.getInstance();
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = FirebaseAuth.getInstance().getCurrentUser();
            }
        };
        /*==================================================*/
        btn_login = (Button)findViewById(R.id.btn_login);
        btn_register = (Button)findViewById(R.id.btn_register);
        l_login = (LinearLayout)findViewById(R.id.l_login);
        txt_got_to_forgot = (TextView)findViewById(R.id.txt_got_to_forgot);
        txt_email = (EditText)findViewById(R.id.txt_email);
        txt_password = (EditText)findViewById(R.id.txt_password);
        r_sect_not_internet_connexion = (RelativeLayout)findViewById(R.id.r_sect_not_internet_connexion);
        l_sect_show_message = (LinearLayout)findViewById(R.id.l_sect_show_message);
        prog_reload = (ProgressBar)findViewById(R.id.prog_reload);
        txt_verify_connexion = (TextView)findViewById(R.id.txt_verify_connexion);
        txt_verify_all_champ = (TextView)findViewById(R.id.txt_verify_all_champ);
        r_my_loader = (RelativeLayout)findViewById(R.id.r_my_loader);
        /*==================================================*/
        /*==================================================*/
        /*txt_phone.setTypeface(Typer.set(getApplicationContext()).getFont(Font.ROBOTO_CONDENSED_REGULAR));
        txt_password.setTypeface(Typer.set(getApplicationContext()).getFont(Font.ROBOTO_CONDENSED_REGULAR));
        btn_register.setTypeface(Typer.set(getApplicationContext()).getFont(Font.ROBOTO_CONDENSED_REGULAR));
        btn_login.setTypeface(Typer.set(getApplicationContext()).getFont(Font.ROBOTO_CONDENSED_REGULAR));
        txt_got_to_forgot.setTypeface(Typer.set(getApplicationContext()).getFont(Font.ROBOTO_CONDENSED_REGULAR));*/
        /*==================================================*/
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                l_login.startAnimation(animSideFadeOut);
                intent = new Intent(getApplicationContext(),Register.class);
            }
        });
        txt_got_to_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                l_login.startAnimation(animSideFadeOut);
                intent = new Intent(getApplicationContext(),Forgot.class);
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable(Login.this)){
                    String tel = txt_email.getText().toString().trim();
                    String password = txt_password.getText().toString().trim();
                    if (tel.equals("") || password.equals("")){
                        niv_back = 1;
                        r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                        txt_verify_all_champ.setVisibility(View.VISIBLE);
                        txt_verify_all_champ.setText("Vous devez indiquer votre adresse email et votre mot de passe!");
                        new CountDownTimer(4000,1000){
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                r_sect_not_internet_connexion.setVisibility(View.GONE);
                                txt_verify_all_champ.setVisibility(View.GONE);
                            }
                        }.start();
                    }else {
                        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                        if (!tel.matches(emailPattern)){
                            niv_back = 1;
                            r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                            txt_verify_all_champ.setVisibility(View.VISIBLE);
                            txt_verify_all_champ.setText("L'adresse email saisie n'est pas valide!");
                            new CountDownTimer(4000,1000){
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    r_sect_not_internet_connexion.setVisibility(View.GONE);
                                    txt_verify_all_champ.setVisibility(View.GONE);
                                }
                            }.start();
                        }else {
                            if (password.length() < 6) {
                                niv_back = 1;
                                r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                                txt_verify_all_champ.setVisibility(View.VISIBLE);
                                txt_verify_all_champ.setText("Le mot de passe indiqué est trop court!");
                                new CountDownTimer(4000,1000){
                                    @Override
                                    public void onTick(long millisUntilFinished) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        r_sect_not_internet_connexion.setVisibility(View.GONE);
                                        txt_verify_all_champ.setVisibility(View.GONE);
                                    }
                                }.start();
                            }else {
                                r_my_loader.setVisibility(View.VISIBLE);
                                l_sect_show_message.setVisibility(View.GONE);
                                prog_reload.setVisibility(View.VISIBLE);
                                r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                                r_sect_not_internet_connexion.setEnabled(false);
                                //Lancer la connexion
                                mAuth.signInWithEmailAndPassword(tel,password).addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (!task.isSuccessful()){
                                            niv_back = 1;
                                            l_sect_show_message.setVisibility(View.VISIBLE);
                                            r_my_loader.setVisibility(View.GONE);
                                            r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                                            r_sect_not_internet_connexion.setEnabled(true);
                                            txt_verify_all_champ.setVisibility(View.VISIBLE);
                                            txt_verify_all_champ.setText("Adresse email ou mot de passe incorrecte!");
                                            new CountDownTimer(4000,1000){
                                                @Override
                                                public void onTick(long millisUntilFinished) {

                                                }

                                                @Override
                                                public void onFinish() {
                                                    r_sect_not_internet_connexion.setVisibility(View.GONE);
                                                    txt_verify_all_champ.setVisibility(View.GONE);
                                                }
                                            }.start();
                                        }else {
                                            user = mAuth.getCurrentUser();
                                            if (user != null) {
                                                sessionManagerClient.storeClientData(user.getEmail());
                                                new CountDownTimer(1500, 500) {

                                                    @Override
                                                    public void onTick(long millisUntilFinished) {

                                                    }

                                                    @Override
                                                    public void onFinish() {
                                                        l_login.startAnimation(animSideFadeOut);
                                                        intent = new Intent(getApplicationContext(), Home.class);
                                                    }
                                                }.start();
                                            }else {
                                                niv_back = 1;
                                                l_sect_show_message.setVisibility(View.VISIBLE);
                                                r_my_loader.setVisibility(View.GONE);
                                                r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                                                r_sect_not_internet_connexion.setEnabled(true);
                                                txt_verify_all_champ.setVisibility(View.VISIBLE);
                                                txt_verify_all_champ.setText("Aucun utilisateur trouvé");
                                                new CountDownTimer(4000,1000){
                                                    @Override
                                                    public void onTick(long millisUntilFinished) {

                                                    }

                                                    @Override
                                                    public void onFinish() {
                                                        r_sect_not_internet_connexion.setVisibility(View.GONE);
                                                        txt_verify_all_champ.setVisibility(View.GONE);
                                                    }
                                                }.start();
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }else{
                    niv_back = 1;
                    btn_login.setEnabled(false);
                    btn_register.setEnabled(false);
                    txt_got_to_forgot.setEnabled(false);
                    txt_password.setEnabled(false);
                    r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
                    txt_verify_connexion.setVisibility(View.VISIBLE);
                }
            }
        });
/*        r_sect_not_internet_connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                niv_back = 0;
                btn_login.setEnabled(true);
                btn_register.setEnabled(true);
                txt_got_to_forgot.setEnabled(true);
                txt_password.setEnabled(true);
                r_sect_not_internet_connexion.setVisibility(View.GONE);
                r_sect_not_internet_connexion.startAnimation(animHide);
                txt_verify_connexion.setVisibility(View.GONE);
            }
        });*/
    }

    /*======================================================*/
    public static boolean isNetworkAvailable(Context context)
    {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }
    //Mes animation
    private void fadeOutAnime(){
        animSideFadeOut = AnimationUtils.loadAnimation(this,R.anim.outgoing);
        animSideFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                l_login.setVisibility(View.GONE);
                startActivity(intent);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void fadeOutAnimeHide(){
        animHide = AnimationUtils.loadAnimation(this,R.anim.outgoing);
        animHide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void fadeinAnime(){
        animFadeIn = AnimationUtils.loadAnimation(this,R.anim.incoming);
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (niv_back == 1) {
                niv_back = 0;
                btn_login.setEnabled(true);
                btn_register.setEnabled(true);
                txt_got_to_forgot.setEnabled(true);
                txt_password.setEnabled(true);
                r_sect_not_internet_connexion.setVisibility(View.GONE);
                r_sect_not_internet_connexion.startAnimation(animHide);
                txt_verify_connexion.setVisibility(View.GONE);
            }else {
                finish();
            }
        }
        return false;
    }
    /*======================================================*/
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthStateListener);
    }
    /*======================================================*/
    //La methode qui permet de logger l'utisateur
    private Clients login_client(String tel,String password){
        Clients cl = new Clients();
        String result = "";
        OkHttpClient client = new OkHttpClient();

        RequestBody body = new FormBody.Builder()
                .add("tel",tel)
                .add("token",token)
                .add("password",password)
                .build();

        Request request = new Request.Builder()
                .url(lien+"clients/login_client.php")
                .post(body)
                .build();
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Response response = client.newCall(request).execute();
            result = response.body().string();
            Log.d(TAG,"Client :"+result);
            if (result != null){
                JSONObject jsonObject = new JSONObject(result);
                cl.setId(jsonObject.optInt("id"));
                cl.setTel(jsonObject.optString("telephone"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  cl;
    }
}
