package com.mbayenn.senauto.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mbayenn.senauto.Helpers.SessionManagerClient;
import com.mbayenn.senauto.Model.Clients;
import com.mbayenn.senauto.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Profil extends AppCompatActivity {
    private static final String TAG = Profil.class.getSimpleName();
    private final String lien = "http://mapi.taxim.ma/taxi_api/";
    private final String token = "XETaxi2017";
    private Animation animSideFadeOut,animFadeIn;
    private RelativeLayout r_profil,r_sect_not_internet_connexion;
    private Intent intent;
    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private CoordinatorLayout main_content;
    private SessionManagerClient sessionManagerClient;
    private CircleImageView img_profil_user;
    private EditText txt_prenom,txt_nom,txt_email;
    private TextView txt_tel;
    private String link;
    private Button btn_reload;
    /*======================================================*/
    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    /*==========================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);
        /*======================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_profil);
        toolbar.setTitle("Profil");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*======================================================*/
        sessionManagerClient = new SessionManagerClient(this);
        r_profil = (RelativeLayout)findViewById(R.id.r_profil);
        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar_profil);
        main_content = (CoordinatorLayout)findViewById(R.id.main_content);
        img_profil_user = (CircleImageView)findViewById(R.id.img_profil_user);
        r_sect_not_internet_connexion = (RelativeLayout)findViewById(R.id.r_sect_not_internet_connexion);
        txt_prenom = (EditText)findViewById(R.id.txt_prenom);
        txt_nom = (EditText)findViewById(R.id.txt_nom);
        txt_email = (EditText)findViewById(R.id.txt_email);
        //txt_tel = (TextView)findViewById(R.id.txt_tel);
        btn_reload = (Button)findViewById(R.id.btn_reload);
        /*======================================================*/
        mAuth = FirebaseAuth.getInstance();
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = FirebaseAuth.getInstance().getCurrentUser();
            }
        };
        /*======================================================*/
        fadeinAnime();
        fadeOutAnime();
        r_profil.startAnimation(animFadeIn);
        /*==================================================================*/
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        /*==================================================================*/
        /*if (isNetworkAvailable(Profil.this)){
            Clients cl = get_client(sessionManagerClient.getLoggedInClient());
            if (!cl.getPhoto().equals("null")) {
                link = "http://mapi.taxim.ma/taxi_api/webroot/clients/" + cl.getPhoto();
            } else {
                link = "http://mapi.taxim.ma/taxi_api/webroot/clients/boy.jpg";
            }
            Glide.with(getApplicationContext()).load(link).fitCenter().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_profil_user);
            txt_prenom.setText(cl.getPrenom());
            txt_nom.setText(cl.getNom());
            txt_email.setText(cl.getEmail());
            txt_tel.setText(cl.getTel());
        }else {
            r_profil.setEnabled(false);
            r_sect_not_internet_connexion.setVisibility(View.VISIBLE);
            r_sect_not_internet_connexion.startAnimation(animFadeIn);
        }*/
        if (mAuth.getCurrentUser() != null){
            txt_email.setText(mAuth.getCurrentUser().getEmail());
            String user_id = mAuth.getCurrentUser().getUid();
            DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Clients").child(user_id);
            current_user_db.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    txt_prenom.setText(dataSnapshot.child("prenom").getValue().toString());
                    txt_nom.setText(dataSnapshot.child("nom").getValue().toString());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        btn_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(),Profil.class);
                r_profil.startAnimation(animSideFadeOut);
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            r_profil.startAnimation(animSideFadeOut);
            main_content.startAnimation(animSideFadeOut);
            intent = new Intent(getApplicationContext(),Home.class);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            r_profil.startAnimation(animSideFadeOut);
            main_content.startAnimation(animSideFadeOut);
            intent = new Intent(getApplicationContext(),Home.class);
        }
        return false;
    }
    /*======================================================*/
    //Mes animation
    private void fadeOutAnime(){
        animSideFadeOut = AnimationUtils.loadAnimation(this,R.anim.outgoing);
        animSideFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                r_profil.setVisibility(View.GONE);
                main_content.setVisibility(View.GONE);
                startActivity(intent);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void fadeinAnime(){
        animFadeIn = AnimationUtils.loadAnimation(this,R.anim.incoming);
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    /*======================================================*/
    public static boolean isNetworkAvailable(Context context)
    {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }
    private Clients get_client(String tel){
        Clients cl = new Clients();
        String result = "";
        OkHttpClient client = new OkHttpClient();

        RequestBody body = new FormBody.Builder()
                .add("tel",tel)
                .add("token",token)
                .build();

        Request request = new Request.Builder()
                .url(lien+"clients/get_client_by_telephone.php")
                .post(body)
                .build();
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Response response = client.newCall(request).execute();
            result = response.body().string();
            Log.d(TAG,"Client :"+result);
            if (result != null){
                JSONObject jsonObject = new JSONObject(result);
                cl.setId(jsonObject.optInt("id"));
                cl.setTel(jsonObject.optString("telephone"));
                cl.setNom(jsonObject.optString("nom"));
                cl.setPrenom(jsonObject.optString("prenom"));
                cl.setDateCreation(jsonObject.optString("dateNaissance"));
                cl.setEmail(jsonObject.optString("email"));
                cl.setPhoto(jsonObject.optString("photo"));
                cl.setStatut(jsonObject.optInt("statut"));
                cl.setEtat(jsonObject.optString("etat"));
                cl.setAdresse(jsonObject.optString("adresse"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cl;
    }
    /*=================================================================*/
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthStateListener);
    }
}
