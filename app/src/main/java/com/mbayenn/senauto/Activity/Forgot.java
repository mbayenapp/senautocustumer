package com.mbayenn.senauto.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.mbayenn.senauto.R;

public class Forgot extends AppCompatActivity {
    private Toolbar toolbar;
    private Animation animFadeIn,animSideFadeOut,animHide;
    private Intent intent;
    private RelativeLayout l_forgot,r_email_sent,r_black;
    private Button btn_send_email,btn_ok,btn_renvoyer;
    private int init_open = 0;
    /*=====================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        /*=================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_forgot);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        /*=================================================*/
        l_forgot = (RelativeLayout) findViewById(R.id.l_forgot);
        r_email_sent = (RelativeLayout)findViewById(R.id.r_email_sent);
        btn_send_email = (Button)findViewById(R.id.btn_send_email);
        btn_ok = (Button)findViewById(R.id.btn_ok);
        btn_renvoyer = (Button)findViewById(R.id.btn_renvoyer);
        r_black = (RelativeLayout)findViewById(R.id.r_black);
        /*=================================================*/
        fadeinAnime();
        fadeOutAnime();
        HideSection();
        if (init_open == 0) {
            l_forgot.startAnimation(animFadeIn);
        }
        /*=================================================*/
        btn_send_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (init_open == 0){
                    init_open = 1;
                }
                btn_send_email.setEnabled(false);
                r_email_sent.setVisibility(View.VISIBLE);
                r_email_sent.startAnimation(animFadeIn);
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (init_open == 1){
                    init_open = 0;
                }
                btn_send_email.setEnabled(true);
                r_email_sent.setVisibility(View.GONE);
                r_email_sent.startAnimation(animHide);
            }
        });
        r_black.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (init_open == 1){
                    init_open = 0;
                }
                btn_send_email.setEnabled(true);
                r_email_sent.setVisibility(View.GONE);
                r_email_sent.startAnimation(animHide);
            }
        });
    }

    /*=====================================================*/
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (init_open == 1){
                init_open = 0;
                btn_send_email.setEnabled(true);
                r_email_sent.setVisibility(View.GONE);
                r_email_sent.startAnimation(animHide);
                //r_email_sent.startAnimation(animHide);
            }else {
                l_forgot.startAnimation(animSideFadeOut);
                intent = new Intent(getApplicationContext(), Login.class);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (init_open == 1){
                init_open = 0;
                btn_send_email.setEnabled(true);
                r_email_sent.setVisibility(View.GONE);
                r_email_sent.startAnimation(animHide);
                //r_email_sent.startAnimation(animHide);
            }else {
                l_forgot.startAnimation(animSideFadeOut);
                intent = new Intent(getApplicationContext(), Login.class);
            }
        }
        return false;
    }
    /*======================================================*/
    //Mes animation
    private void fadeinAnime(){
        animFadeIn = AnimationUtils.loadAnimation(this,R.anim.incoming);
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void fadeOutAnime(){
        animSideFadeOut = AnimationUtils.loadAnimation(this,R.anim.push_left_out);
        animSideFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                l_forgot.setVisibility(View.GONE);
                startActivity(intent);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void HideSection(){
        animHide = AnimationUtils.loadAnimation(this,R.anim.outgoing);
        animHide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    /*======================================================*/
}
