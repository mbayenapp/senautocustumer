package com.mbayenn.senauto.Modules;

import java.util.List;

/**
 * Created by Mbaye on 18/03/2017.
 */

public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
