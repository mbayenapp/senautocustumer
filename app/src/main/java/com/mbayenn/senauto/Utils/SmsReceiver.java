package com.mbayenn.senauto.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by Mbaye on 14/04/2017.
 */

public class SmsReceiver extends BroadcastReceiver {
    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data  = intent.getExtras();

        Object[] pdus = (Object[]) data.get("pdus");


        for(int i=0;i<pdus.length;i++){
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);

            String sender = smsMessage.getDisplayOriginatingAddress();
            //You must check here if the sender is your provider and not another one with same text.


            String messageBody = smsMessage.getMessageBody();
                //Pass on the text to our listener.
            if (messageBody.length() == 34){
                mListener.messageReceived(messageBody);
                Log.d("body", String.valueOf(messageBody.length()));
            }
        }

    }

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }
}
