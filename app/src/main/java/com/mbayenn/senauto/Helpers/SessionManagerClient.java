package com.mbayenn.senauto.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mbaye on 09/03/2017.
 */

public class SessionManagerClient {
    private static String TAG = SessionManagerClient.class.getSimpleName();
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Shared preferences file name
    private static final String PREF_NAME = "LoginClientDetails";
    private static final String KEY_IS_LOGGEDIN = "statutClient";

    public SessionManagerClient(Context _context) {
        this._context = _context;
        pref = _context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
    }
    public void storeClientData(String email){
        editor = pref.edit();
        editor.putString("email",email);
        editor.commit();
    }
    public  String getLoggedInClient(){
        String tel = pref.getString("email","");
        return tel;
    }

    public void setLoggedInClientLogin(boolean loggedIn){
        editor = pref.edit();
        editor.putBoolean("LoggedIn",loggedIn);
        editor.commit();
    }
    public void clearClientLoginData(){
        editor = pref.edit();
        editor.clear();
        editor.commit();
    }
    public boolean isLoggedIn(){
        String email = getLoggedInClient();
        boolean statut = pref.getBoolean(KEY_IS_LOGGEDIN, false);
        if (email != ""){
            statut = pref.getBoolean(KEY_IS_LOGGEDIN, true);
        }
        return statut;
    }
}
