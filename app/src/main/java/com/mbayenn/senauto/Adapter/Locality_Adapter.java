package com.mbayenn.senauto.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mbayenn.senauto.Model.Locality;
import com.mbayenn.senauto.R;

import java.util.List;

/**
 * Created by Mbaye on 04/10/2017.
 */

public class Locality_Adapter extends BaseAdapter {
    private Context context;
    List<Locality> localities;
    private LayoutInflater layoutInflater;

    public Locality_Adapter(Context context, List<Locality> localities) {
        this.context = context;
        this.localities = localities;
    }

    @Override
    public int getCount() {
        return localities.size();
    }

    @Override
    public Object getItem(int position) {
        return localities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        convertView = layoutInflater.inflate(R.layout.locality_item, null);
        TextView txt_locality = (TextView)convertView.findViewById(R.id.txt_name_locality);
        TextView txt_ville = (TextView)convertView.findViewById(R.id.txt_ville_locality);
        TextView txt_pays = (TextView)convertView.findViewById(R.id.txt_pays_locality);
        Locality locality = localities.get(position);
        txt_locality.setText(locality.getAdresse());
        txt_ville.setText(locality.getVille());
        txt_pays.setText(locality.getPays());
        return convertView;
    }
}
