package com.mbayenn.senauto.Model;

/**
 * Created by Mbaye on 04/10/2017.
 */

public class Locality {
    private String id,adresse,ville,pays;

    public Locality() {
    }

    public Locality(String id, String adresse, String ville, String pays) {
        this.id = id;
        this.adresse = adresse;
        this.ville = ville;
        this.pays = pays;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }
}
