package com.mbayenn.senauto.Model;

/**
 * Created by Mbaye on 12/07/2017.
 */

public class Points {
    private double lat,lng;

    public Points() {
    }

    public Points(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
